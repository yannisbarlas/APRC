 
import java.util.Scanner ;
 
public class VendingSystem {
       
        static Scanner sc = new Scanner(System.in) ;
        static int choice, money=500, total, change, extra ;
        static String receipt = "\nITEM\t\tCOST\tTotal";
        static boolean run = true ;
       
        static VendingItem chocolate = new VendingItem("Chocolate", 57, 5) ;
        static VendingItem cola = new VendingItem("Cola", 71, 3) ;
        static VendingItem crisps = new VendingItem("Crisps", 34, 4) ;
        static VendingItem currypot = new VendingItem("Curry pot", 149, 2) ;
 
        public static void main(String[] args) {
               
                // what if we don't have an int as input for scanner?
               
                do {
                        doMenu() ;
                        System.out.print("\nPlease choose one of the options above: ") ;
                        choice = sc.nextInt() ;
                       
                        switch (choice) {
                                case 1: buyItem(chocolate); break;
                                case 2: buyItem(cola); break;
                                case 3: buyItem(crisps); break;
                                case 4: buyItem(currypot); break;
                                case 5: completeSale(); break;
                                case 6: insertPound() ; break;
                                case 0: noSale() ; break;
                                default: System.err.println("Invalid choice!") ; break;
                        }
                       
                       
                } while (run) ;
               
        }
       
        private static void doMenu() {
               
                System.out.println("[1]\t" + chocolate.toString()) ;
                System.out.println("[2]\t" + cola.toString()) ;
                System.out.println("[3]\t" + crisps.toString()) ;
                System.out.println("[4]\t" + currypot.toString()) ;
                System.out.println("[5]\tComplete Sale") ;
                System.out.println("[6]\tInsert Pound") ;
                System.out.println("[0]\tQuit") ;
               
        }
       
        private static void buyItem(VendingItem v) {
                if (money > v.getItemPrice()) {
                        if (v.stockLeft()) {
                               
                                v.buyItem() ;
                                money -= v.getItemPrice() ;
                                change = money - v.getItemPrice() ;
                                addToReceipt(v.getItemName(), v.getItemPrice()) ;
                               
                                System.out.println("\nYou bought " + v.getItemName() + "!") ;
                                System.out.println("Money left: " + money/100 + "." + money%100 + ".\n") ;
 
                        }
                        else { System.out.println("Item out of stock!\n") ; }
                }
                else { System.out.println("You don't have enough money to purchase this item!\n") ;}
               
        }
       
        private static void addToReceipt(String item, int price) {
                total += price ;
                receipt += "\n" +item + "\t" + price/100 + "." + price%100 + "\t"  + total/100 + "." + total%100 ;
        }
       
        private static void displayReceipt() {
                System.out.println(receipt) ;
                System.out.println("\nChange due: �" + change/100 + "." + change%100) ;
                getLowestCoins(change) ;
        }
       
        private static void getLowestCoins(int c) {
                int five_pounds = c / 500 ;
		        if (five_pounds != 0) {c = c - five_pounds*500 ;}
		        int two_pounds = c / 200 ;
		        if (two_pounds != 0) {c = c - two_pounds*200 ;}
                int pound  = c / 100 ;
		        if (pound != 0) {c = c - pound*100 ;}
		        int fifty  = c / 50  ;
		        if (fifty != 0) {c = c - fifty*50 ;}
		        int twenty = c / 20  ;
		        if (twenty != 0) {c = c - twenty*20 ;}
		        int ten    = c / 10  ;
		        if (ten != 0) {c = c - ten*10 ;}
		        int five   = c / 5   ;
		        if (five != 0) {c = c - five*5 ;}
		        int two    = c / 2   ;
		        if (two != 0) {c = c - two*2 ;}
		        int one    = c / 1   ;
		       
		        System.out.println("\n�5:\t" + five_pounds) ;
		        System.out.println("�2:\t" + two_pounds) ;
		        System.out.println("�1:\t" + pound) ;
		        System.out.println("50p:\t" + fifty) ;
		        System.out.println("20p:\t" + twenty) ;
		        System.out.println("10p:\t" + ten) ;
		        System.out.println("5p:\t" + five) ;
		        System.out.println("2p:\t" + two) ;
		        System.out.println("1p:\t" + one) ;
        }
       
       
        private static void completeSale() {
                displayReceipt() ;
                run = false ;
        }
       
        private static void noSale() {
                money = 500 + extra ;
                System.out.println("You didn't buy anything.") ;
                getLowestCoins(money) ;
                run = false ;
        }
       
        private static void insertPound() {
                extra += 100 ;
                money += 100 ;
                System.out.println("You now have " + money/100 + "." + money%100 + " pounds.\n") ;
        }
}
 
 
// format tabs in menu and receipt

/* 
 * I didn't implement the recommended doSale() method, as I incorporated its functionality in other methods. 
 * Same as before, I could used a method to format pounds and pence, instead of manually doing it every time.
 * Exercise 7 wasn't implemented, as I didn't know much about inheritance in java.
 * Exercise 8 wouldn't really make sense in my version of the vending machine, as it breaks the doMenu() loop when a user quits. 
 */