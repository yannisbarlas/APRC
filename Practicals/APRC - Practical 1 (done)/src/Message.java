 
public class Message {
       
        //private String message ;
		String message ;
        static Message[] allMessages = new Message[3] ;
        
        public Message (String s) { message = s ; }
       
        public void printMessage() { System.out.println(message) ; }
        
        // This method didn't work on the multiple append line in main, because I was trying to return a String, while I needed to return an object to make it work  
        // public String append(String anotherString) {
        public Message append(String anotherString) {
                message = message + anotherString ;
        		return this ;
        }
       
        public String toString() {
                return String.format("Message with message text '%s'.", this.message) ;
        }
        
        static public class MessagePrinter  {
        	public void addMessage(Message m, int position) {
        		allMessages[position] = m ;
        	}
        	
        	public void printAllMessages() {
        		int l = allMessages.length ;
        		int count = 0 ;
        		while (count < l) {
        			allMessages[count].printMessage();
        			count ++ ;
        		}
        	}
        }
        
//      ##############################################################
//      ########################	MAIN	##########################
//      ##############################################################
        
        public static void main( String[] args ) {
            
    		// Exercise 2
    		Message m1 = new Message("test 1");
            m1.printMessage();
            Message m2 = new Message("test 2");
            m2.printMessage() ;
            m1.printMessage();
            
            // Exercise 3
            Message m = new Message("test");
            m.append(" in").append(" progress").append(" ... ");
            m.printMessage();
            
            // Exercise 4
            System.out.println(m.toString()) ;
    
            // Exercise 5
            MessagePrinter mp = new MessagePrinter() ;
            
            mp.addMessage(m1, 0) ;
            mp.addMessage(m2, 1) ;
            mp.addMessage(m , 2) ;
            
            mp.printAllMessages() ;
    }
       
}