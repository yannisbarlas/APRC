 
public class VendingItem {
 
        private String name ;
        private int price, stock ;
 
        public VendingItem(String n, int p, int s) {
                name = n ;
                price = p;
                stock = s ;
        }
       
        public String getItemName() { return this.name ; }
       
        public int getItemPrice () { return this.price ; }
       
        public boolean stockLeft() {
                if (stock>0) {return true ;}
                else {return false ;}
        }
       
        public void buyItem() { this.stock -- ; }
       
        public String toString() { return this.getItemName() + "\tprice: " + this.getItemPrice()/100 + "." + this.getItemPrice()%100 + "\tstock = " + this.stock ; }
}