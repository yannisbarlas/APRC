public class TimesTable {
	
	String[] header ;
	Object[][] rows ;
	int number ;
	
	// Object constructor
	public TimesTable(int n) {
		number 	= n  			;	// Make sure you only get the number you will use on the table
		header 	= makeHeader(n) ;	// Use method to get column headers
		rows 	= makeRows(n) 	;	// Use method to get table content
	}
	
	// METHODS RETURN STRING[] AND OBJECT[][], AS THESE ARE THE TYPES THAT JTABLE IS EXPECTING
	
	// Method that creates column headers
	private String[] makeHeader(int n) {
		String[] s = new String[n+1] ;		// n+1 => all numbers, plus the "*" sign in the beginning
		for ( int i=0 ; i<=n ; i++ ) {
			if (i==0) { s[i] = "*" ; }		// Create "*" sign in first cell
			else { s[i] = "" + i ; }		// Name columns 1 to n, as 1,2,3...n
		}
		return s ;
	}
	
	//Method that creates table content
	private Object[][] makeRows(int n) {
		Object[][] r = new Object[n][n+1] ;			// Make an object that has n lines, but each line has n cells plus the "header" cell on the left. If not clear, try printing it. 
		for ( int j=0 ; j<=(n-1) ; j++ ) {			// n lines, so counting from 0 to n-1
			for ( int k=0 ; k<=n ; k++ ) {			// n+1 cells in each line, so counting from 0 to n
				if (k==0) { r[j][k] = j+1 ; }		// Since j was initialized at 0 instead of 1, always use j+1 for the values (but j for the positions)
				else { r[j][k] = (j+1)*k ; }
			}
		}
		return r ; 
	}
	
	public String[] getHeader() { return this.header ; }	// Gets header
	public Object[][] getRows() { return this.rows ; }		// Gets content
}

