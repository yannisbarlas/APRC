import java.awt.event.* ;

import javax.swing.* ;

import java.util.ArrayList ;
import java.util.Set ;
import java.util.HashSet ;

public class gui_vending {
	
	// Declarations
	static int left=500, extra, change=0, total ;
	static String receipt = "\nITEM\tCOST\tTotal\n", lowest ;
	
	@SuppressWarnings("rawtypes")
	JList list ;
	JPanel panel ;
	static JLabel money, fiveP, twoP, oneP, fiftyp, twentyp, tenp, fivep, twop, onep ;
	private static JTextArea recpt ;
	private static JFrame window ;
	
	static ArrayList<Object> instances = new ArrayList<Object>() ;
	static Set<VendingItem> canceledItems = new HashSet<VendingItem>() ;
	 
	// Object instances
	static VendingItem Chocolate = new VendingItem("Chocolate", 57, 5) ;
    static VendingItem Cola = new VendingItem("Cola", 71, 3) ;
    static VendingItem Crisps = new VendingItem("Crisps", 34, 4) ;
    static VendingItem Currypot = new VendingItem("Curry pot", 149, 2) ;
    static VendingItem Orangejuice = new VendingItem("Orange juice", 82, 4) ;
    static VendingItem Popcorn = new VendingItem("Pop corn", 115, 2) ;

	// Call setUpGUI()
	private gui_vending() {
		setUpGUI() ;
	}
	
//	################################################
//	################	GUI		####################
//	################################################
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void setUpGUI() {
		// Set window basics
		window = new JFrame("Vending Machine")					;
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)	;
		
		panel = new JPanel() 		;
		window.setContentPane(panel);
		
		// Create list of items that can be bought
		addItems() 								;  // populate the array list
		list = new JList(instances.toArray())	;  // add it to jlist
		list.setBounds(26, 35, 239, 154)		;  
		panel.add(list)							;
		
		addBuyButton() ;
		
		// Create money left label
		money = new JLabel("Money left: �" + left/100 + "." + left%100) ;
		money.setBounds(77, 229, 133, 16);
		window.getContentPane().add(money) ;
		
		addInsertButton() 	;
		addCompleteButton() ;
		addCancelButton() 	;
		
		// Create text area for receipt and no sale messages
		recpt = new JTextArea();
		recpt.setBounds(51, 362, 309, 394);
		panel.add(recpt);
		
		// Show window
		window.setSize(400,800) ;
		window.setVisible(true);
	}
	
	private void addItems() {
		instances.add(Chocolate) ;
		instances.add(Cola) ;
		instances.add(Crisps) ;
		instances.add(Currypot) ;
		instances.add(Orangejuice) ;
		instances.add(Popcorn) ;
	}
	
	private void addBuyButton() {
		JButton buy = new JButton("Buy");
		buy.setBounds(277, 119, 117, 29);
		buy.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				@SuppressWarnings("deprecation")
				Object[] chosen = list.getSelectedValues() ;		// make a list of all selected values
				for ( int i=0 ; i<chosen.length ; i++ ) {			// for each item in that list
					VendingItem current = (VendingItem) chosen[i] ;	// force the item to be a vending item instance
					buyItem(current) ;								// buy it
				}
			}
		});
		panel.add(buy);
	}
	
	private void addInsertButton() {
		JButton insert = new JButton("Insert pound") ;
		insert.setBounds(236, 224, 124, 29);
		insert.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				insertPound() ;
			}
		});
		panel.setLayout(null);
		window.getContentPane().add(insert) ;
	}
	
	private void addCompleteButton() {
		JButton complete = new JButton("Complete Sale") ;
		complete.setBounds(132, 280, 133, 29);
		complete.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				displayReceipt() ;
			}
		});
		window.getContentPane().add(complete) ;
	}
	
	private void addCancelButton() {
		JButton cancel = new JButton("Cancel");
		cancel.setBounds(154, 321, 86, 29);
		cancel.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				noSale() ;
			}
		});
		panel.add(cancel);
	}
	
	
	
//	################################################
//	##############     METHODS     #################
//	################################################	
	
	// Lets user increase his money by one pound
	private static void insertPound() {
		extra += 100 ;													// keep track of inserted pounds, in case the user hits cancel
		left += 100 ;													// increase user's money
		money.setText("Money left: �" + left/100 + "." + left%100) ;	// update money label
	}
	
	// Checks if there is enough money and stock for the purchase and buys item
	private static void buyItem(VendingItem v) {
        if (left > v.getItemPrice()) {
                if (v.stockLeft()) {
                        v.buyItem() ;													// method from vendingItem object
                        left -= v.getItemPrice() ;										// reduce money left by item's price
                        money.setText("Money left: �" + left/100 + "." + left%100) ;	// update money label
                        addToReceipt(v.getItemName(), v.getItemPrice()) ;				// add data to receipt
                        canceledItems.add(v) ;
                }
                else { JOptionPane.showMessageDialog(window, "Item out of stock!", "Out of stock", JOptionPane.PLAIN_MESSAGE); }
        }
        else { JOptionPane.showMessageDialog(window, "You don't have enough money to purchase this item!", "Insufficient amount of money", JOptionPane.PLAIN_MESSAGE);}
    }
	
	// Adds bought items to our receipt, so that it can be printed later
	private static void addToReceipt(String item, int price) {
        total += price ;
        receipt += "\n" +item + "\t" + price/100 + "." + price%100 + "\t"  + total/100 + "." + total%100 ;
    }
	
	// Calculates change and prints out receipt and the change breakdown
	private static void displayReceipt() {
        getLowestCoins(left) 			;
        recpt.setText(receipt + lowest) ;
	}
	
	// Figures out the change breakdown
	private static void getLowestCoins(int c) {
        // Calculate change for each coin / note
		int five_pounds = c / 500 ;
        if (five_pounds != 0) {c = c - five_pounds*500 ;}
        int two_pounds = c / 200 ;
        if (two_pounds != 0) {c = c - two_pounds*200 ;}
        int pound  = c / 100 ;
        if (pound != 0) {c = c - pound*100 ;}
        int fifty  = c / 50  ;
        if (fifty != 0) {c = c - fifty*50 ;}
        int twenty = c / 20  ;
        if (twenty != 0) {c = c - twenty*20 ;}
        int ten    = c / 10  ;
        if (ten != 0) {c = c - ten*10 ;}
        int five   = c / 5   ;
        if (five != 0) {c = c - five*5 ;}
        int two    = c / 2   ;
        if (two != 0) {c = c - two*2 ;}
        int one    = c / 1   ;
        
        // Add all of that to the string lowest
        lowest = "\n\nChange due: �" + left/100 + "." + left%100 + "\n" ;
        lowest += "\n�5:\t" + five_pounds ;
        lowest += "\n�2:\t" + two_pounds ;
        lowest += "\n�1:\t" + pound ;
        lowest += "\n50p:\t" + fifty ;
        lowest += "\n20p:\t" + twenty ;
        lowest += "\n10p:\t" + ten ;
        lowest += "\n5p:\t" + five ;
        lowest += "\n2p:\t" + two ;
        lowest += "\n1p:\t" + one ;
	}
	
	// Lets the user cancel the whole thing
	private static void noSale() {
	        left = 500 + extra 										;		// Remember to keep track of inserted pounds as well.
	        getLowestCoins(left) 									;
	        recpt.setText("You didn't buy anything." + lowest)		;
	        for ( VendingItem v : canceledItems ) { v.resetStock() 	; }		// Reset stock to initial value for all items that had been bought, but canceled instead of completed.
	}
	
	
//	################################################
//	################	MAIN	####################
//	################################################
	@SuppressWarnings("unused")
	public static void main (String[] args) {
		gui_vending vending = new gui_vending() ;
	}
}


/* 
 * Design in the list with tab spaces is not exactly great.
 * Gui glitch : when resetting stock after cancel, I have to click on item for it to display correct stock.
 */
