import java.awt.event.* ;

import javax.swing.* ;

public class Table {
	
	JFrame window 		;
	JTextField number 	;
	JTable table 		;
	JScrollPane scroll 	;
	
	private Table() {
		Gui() ;
	}
	
//	##########################################################
//	########################  GUI  ###########################
//	##########################################################
	
	public void Gui() {
		// Window basics
		window = new JFrame("Times Table") 						;
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)	;
		
		JPanel panel = new JPanel() 	;
		window.setContentPane(panel)	;
		panel.setLayout(null)			;
		
		// User input prompt and input text field
		JLabel please = new JLabel("Please enter an integer:") 	;
		please.setBounds(131, 11, 153, 16)						;
		window.getContentPane().add(please) 					;
		
		number = new JTextField("") 		;
		number.setBounds(316, 5, 41, 28)	;
		window.getContentPane().add(number) ;
		
		// Make table button --- functionality in separate method
		makeButton() ;
		
		// Scroll pane where table will be inserted
		scroll = new JScrollPane()			;
		scroll.setBounds(37, 59, 725, 493)	;
		panel.add(scroll)					;
		
		// Show window
		window.setSize(800,600) ;
		window.setVisible(true)	;
	}
	
	// Method that creates the make button and its functionality
	private void makeButton() {
		JButton make = new JButton("Make table!") 	;			
		make.setBounds(562, 6, 116, 29)				;
		make.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				makeTable(number.getText()) ;					// When button is pressed, get text field input and trigger make table method
			}
		});
		window.getContentPane().add(make) ;
	}
	
	// Method that creates the table and adds it to the scroll pane
	private void makeTable(String s) {
		try { 
			int num = Integer.parseInt(s) 							;		// Convert text input into integer --- if not possible, throw error window as described below
			TimesTable t = new TimesTable(num) 						;		// Make Times Table object instance called t
			JTable table = new JTable(t.getRows(), t.getHeader()) 	;		// Make a table with object instances values as parameters
			scroll.setViewportView(table)							;		// Add table to scroll pane
		} 
		catch (NumberFormatException e) { JOptionPane.showMessageDialog (window, "Please enter an integer!", "Error in input", JOptionPane.PLAIN_MESSAGE) ; }
	}
	
	
//	##########################################################
//	#######################  MAIN  ###########################
//	##########################################################	
	
	@SuppressWarnings("unused")
	public static void main ( String[] args0 ) {
		Table w = new Table() ;
	}
}
