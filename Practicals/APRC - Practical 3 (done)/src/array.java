 
import java.util.Arrays 	;
import java.util.ArrayList 	;
import java.util.Scanner 	;
 
public class array {
       
        static int[] numbers = {1, 7, 3, 9, 5, 0, 8, 2, 6, 4}				;
        static int i, result, temp 											;
        static ArrayList<Integer> numbersList = new ArrayList<Integer>() 	;
       
//      #####################################################  
//      ####################  METHODS  ######################
//      #####################################################
        
        // Calculate the sum of the elements
        public static int sum(int[] a) {
        		result = 0 ;
                for (i=0 ; i<a.length ; i++) { result += a[i] ; }
        		return result ;
        }
       
        // Calculate the mean of all elements
        public static double mean(int[] a) { return ( (double) sum(a) / a.length ) ; }
       
        // Print the array
        public static void printArray (int[] a) { for (i=0 ; i < a.length ; i++) { System.out.print(a[i] + " ") ; } }
       
        // Sort the array in descending order using bubble sort
        public static void bubble(int[] a) {
                boolean on = true ;
               
                while (on) {
                        on = false ;
                        for (i=0 ; i < (a.length -1) ; i++) {
                        	if (a[i] < a[i+1]) {
                        		on = true 		;
                            	temp = a[i] 	;
                            	a[i] = a[i+1] 	;
                            	a[i+1] = temp 	;
                        	}
                        	
                        }
                }
        }
       
        // Sort the array list in ascending order using bubble sort
        public static void bubble4List (ArrayList<Integer> a) {
	        	boolean on = true ;
	        	
	        	while (on) {
	        		on = false ;
	        		for (i=0 ; i < (a.size() - 1) ; i++ ) {
	        			if (a.get(i) > a.get(i+1)) {
	        				on = true ;
	        				temp = a.get(i) 	;
	        				a.set(i,a.get(i+1)) ;
	        				a.set(i+1, temp) 	;
	        			}
	        		}
	        	}
        }
       
        // Take an array and make an array list out of it
        public static void makeArray(int[] a) { for (i=0 ; i < a.length ; i++) { numbersList.add(a[i]) ; } }
        
        // Binary search on an array list using recursion
        public static void binarySearch(ArrayList<Integer> list, int input, int pivot) {
        	int l = list.size();
        	int value = list.get(pivot) ;
        	
        	if ( ( pivot==0 | pivot==l-1 )  &&  input != value ) { System.out.println ("User input is not the array.") ; }  // Break recursion loop if input is not in the list!  
        	else {
        		if (input == value) { System.out.println ("User input can be found in position " + pivot +".") ; }
            	else if (input < value) {
            			pivot = pivot / 2 ;																					// Update pivot if pivot is too large.
            			binarySearch(list, input, pivot) ;
            		}
            	else {
    	        		pivot = ((l- pivot) / 2) + pivot ;																	// Update pivot if pivot is too small.
    	        		binarySearch(list, input, pivot) ;
            	}
        	}
        	
        }
        
//      #####################################################  
//      ######################  MAIN  #######################
//      #####################################################
       
        public static void main(String[] args) {
                // Make an array list out of the array
                makeArray(numbers);
                System.out.println(numbersList) ;
               
                // Display sum
                System.out.println("The sum of the elements is " + sum(numbers)) ;
               
                // Display mean
                System.out.println("The mean of the elements is " + mean(numbers)) ;
               
                // Display sorted array
                Arrays.sort(numbers)	;
                printArray(numbers) 	;
                System.out.println() 	;
               
                // Display array in reverse order
                bubble(numbers) 		;
                printArray(numbers) 	;
                System.out.println()	;
               
                // Display sorted array list
                bubble4List(numbersList) 		;
                System.out.println(numbersList) ;
                
                // Implement binary search based on user's input
                Scanner sc = new Scanner(System.in) 							;
                System.out.print("Please input an integer: ") 					;
                if (sc.hasNextInt()) { 
                	int n = sc.nextInt()					; 
                	int sz = numbersList.size() 			;
                	binarySearch(numbersList, n, (sz/2)) 	;
                }
                else { System.out.println ("Please enter an integer!") ; }
                sc.close();
        }
 
}
