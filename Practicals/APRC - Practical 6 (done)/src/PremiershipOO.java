import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
 
 
public class PremiershipOO {
       
		static ArrayList<String[]> lines = new ArrayList<String[]> () ;
        static ArrayList<String> allTeams = new ArrayList<String> () ;
        static ArrayList<String[]> teamLines = new ArrayList<String[]> () ;
        private JTable table, table2;
        
        static ArrayList<Team> teamObjects = new ArrayList<Team> () ;
       
        JFrame window ;
        JScrollPane scrollPane, scrollPane2 ;
        
//		############################################
//		#################   GUI   ##################
//		############################################        
        
        private PremiershipOO() {
                gui() ;
        }
       
        public void gui() {
        		// Window basics
                window = new JFrame() ;
                window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
                JPanel panel = new JPanel() ;
                window.setContentPane(panel) ;
                panel.setLayout(null);
               
                // Make label and info table
                JLabel lblTeamInformation = new JLabel("Team Information");
                lblTeamInformation.setBounds(64, 12, 172, 15);
                panel.add(lblTeamInformation);
               
                scrollPane = new JScrollPane();
                scrollPane.setBounds(64, 61, 565, 275);
                panel.add(scrollPane);
               
                table = new JTable(makeRows(),makeHeader());
                scrollPane.setViewportView(table);
                
                // Make label and league table
                JLabel lblLeagueTable = new JLabel("League Table");
                lblLeagueTable.setBounds(64, 412, 172, 15);
                panel.add(lblLeagueTable);
               
                scrollPane2 = new JScrollPane();
                scrollPane2.setBounds(64, 461, 565, 275);
                panel.add(scrollPane2);
               
                table2 = new JTable(makeLeagueRows(),makeLeagueHeader());
                scrollPane2.setViewportView(table2);
                
                // Make add match feature
                JButton addMatch = new JButton("Add match");
                addMatch.setBounds(64, 800, 100, 20);
                addMatch.addActionListener (new ActionListener() {
                	@Override
                	public void actionPerformed (ActionEvent arg0) {
                		@SuppressWarnings("unused")
						match mw = new match() ;
                	}
                });
                panel.add(addMatch) ;
               
                // Open window
                window.setSize(800,900) ;
                window.setVisible(true) ;
        }
        
        
        
//		############################################
//		#############    METHODS    ################
//		############################################
       
        // Reads the file and creates an arraylist out of it
        public static void reader() {
        		
        		String[] s ;
        	
                try {
                        BufferedReader r = new BufferedReader (new FileReader("premiership.txt")) ;
                        Scanner sc = new Scanner(r).useDelimiter(";") ;
                        while (sc.hasNextLine()) {
                        		s = (sc.nextLine()).split(";") ;
                                lines.add(s) ;
                        }
                        
                        for ( int i=0 ; i<lines.size() ; i++ ) {
                        	Team t = new Team(lines.get(i)[0], Integer.parseInt(lines.get(i)[1]), Integer.parseInt(lines.get(i)[2]), Integer.parseInt(lines.get(i)[3]), Integer.parseInt(lines.get(i)[4])) ;
                        	teamObjects.add(t) ;
                        }
                       
                }
                catch (FileNotFoundException e) { e.printStackTrace() ; }
        }
       
        // Method that saves current team stats into a new file
        public static void save() {
                try {
                        BufferedWriter w = new BufferedWriter (new FileWriter("newPremiership.txt")) ;
                        for (Team t : teamObjects) {
                        	w.write( t.toString() + ";" + t.getGamesPlayed() + ";" + t.getGoalsPro() + ";" + t.getGoalsCon() + ";" + t.getPoints() + "\n" );
                        }
                        w.close();
                }
                catch (IOException e) { e.printStackTrace() ; }
        }
       
        // Makes the header for the info table
        public static String[] makeHeader() {
                return new String[] {"Team Name", "Games Played", "Goals Scored", "Goals Conceded", "Points" } ;
        }
       
        // Makes the data object for the info table
        public static Object[][] makeRows() {
                Object[][] o = new Object[teamObjects.size()][5] ;	// Create an object the size of the info table
                
                for ( int i=0 ; i<teamObjects.size() ; i++ ) {		// For each team
                        for ( int j=0 ; j<5 ; j++ ) {
                        	switch (j) {							// Populate table with appropriate info
                        		case 0 : o[i][j] = teamObjects.get(i) 					; break;
                        		case 1 : o[i][j] = teamObjects.get(i).getGamesPlayed()	; break;
                        		case 2 : o[i][j] = teamObjects.get(i).getGoalsPro() 	; break;
                        		case 3 : o[i][j] = teamObjects.get(i).getGoalsCon() 	; break;
                        		case 4 : o[i][j] = teamObjects.get(i).getPoints() 		; break;
                        	}
                        	
                        }
                }
                
                return o;											// Return object
        }
       
        // Makes an arraylist of the teams and their data in a sorted order (according to their points in the league)
        private static ArrayList<Team> makePointsArrayList() {
                
                // Sort new arraylist by points and goal difference (using bubble sort)
                boolean keep = true;
                Team temp ;
                
                while (keep) {
                	keep = false ;
                	
                	for ( int i=0 ; i < teamObjects.size()-1 ; i++) {
                		
                		// Get each team's array of data
                		Team team1 = teamObjects.get(i) ;
                		Team team2 = teamObjects.get(i+1) ;
                		
                		// Get each team's points
                		int points1 = team1.getPoints() ;
                		int points2 = team2.getPoints() ;
                		
                		// Get each team's goal difference
                		int goals1 = team1.getGoalsDifference() ;
                		int goals2 = team2.getGoalsDifference() ;
                		
                		// If the first team has less points than the second one, switch them around
                		// If they have the same points, sort by goal difference
                		if (points1 < points2 | ( points1 == points2 && goals1 < goals2 ) ) {
                			temp = team1 ;
                			teamObjects.set(i, team2) ;
                			teamObjects.set(i+1, temp) ;
                			keep = true ;				// If the conditions are never met in the whole for loop, keep is left as false, breaking the while loop
                		}
                	}
                }
                
                // Give ranking number to each team in the (now) sorted arraylist
                int rank = 1 ;
                
                for (int i=0 ; i<teamObjects.size(); i++) {
                	teamObjects.get(i).setRank(rank) ;
                	rank ++ ;
                }
                
                // Return sorted arraylist
                return teamObjects ;
        }
       
        // Makes the header for the league table
        private String[] makeLeagueHeader() {
        	return new String[] {"Rank", "Team", "Goals difference", "Points"} ;
        }
        
        // Makes the data object for the league table
        private Object[][] makeLeagueRows() {
        	ArrayList<Team> leagueList = makePointsArrayList() ;		// Get the sorted arraylist
        	Object[][] o2 = new Object[leagueList.size()][4] ;			// Make an object the size of the league table
        	
        	for ( int j=0 ; j<leagueList.size() ; j++ ) {				// For each team
        		Team t = leagueList.get(j) ;
        		String[] teamLine = new String[] {(t.getRank()+""), t.getName(), (t.getGoalsDifference()+""), (t.getPoints()+"")} ; // Set teamLine as the team data
        		for ( int k=0 ; k<4 ; k++ ) {							
        			o2[j][k] = teamLine[k] ;							// Get each of the data in the appropriate box
        		}
        	}
        	
        	return o2 ;													// Return object
        }
        
        // Makes a new window to add a new match
        @SuppressWarnings("serial")
		class match extends JFrame {
        	
        	private JFrame matchWindow;
        	private JTextField home, away;
        	private JList<Team> homeList, awayList ;
        	
        	public match() {
        		// Window basics
        		matchWindow = new JFrame() ;
        		JPanel matchPanel = new JPanel() ;
        		matchWindow.setContentPane(matchPanel);
        		matchPanel.setLayout(null);
        		
        		// Make a list model, then populate the two jlists
        		DefaultListModel<Team> md = new DefaultListModel<Team> () ;
        		for (Team t : teamObjects) { md.addElement(t); } 
        		
        		homeList = new JList<Team> (md);
        		homeList.setBounds(72, 52, 257, 345);
        		matchPanel.add(homeList);
        		
        		awayList = new JList<Team> (md);
        		awayList.setBounds(465, 52, 257, 345);
        		matchPanel.add(awayList);
        		
        		// Make labels and text fields for new match's score results
        		JLabel lblHome = new JLabel("Home");
        		lblHome.setBounds(183, 24, 61, 16);
        		matchPanel.add(lblHome);
        		
        		JLabel lblAway = new JLabel("Away");
        		lblAway.setBounds(572, 24, 61, 16);
        		matchPanel.add(lblAway);
        		
        		JLabel lblScore = new JLabel("Score");
        		lblScore.setBounds(381, 403, 61, 16);
        		matchPanel.add(lblScore);
        		
        		home = new JTextField();
        		home.setBounds(195, 420, 134, 28);
        		matchPanel.add(home);
        		home.setColumns(10);
        		
        		away = new JTextField();
        		away.setBounds(465, 420, 134, 28);
        		matchPanel.add(away);
        		away.setColumns(10);
        		
        		// Make button that adds match results into the teams' data
        		JButton btnAddMatch = new JButton("Add Match!");
        		btnAddMatch.setBounds(347, 490, 117, 29);
        		btnAddMatch.addActionListener( new ActionListener() {
        			@Override
        			public void actionPerformed (ActionEvent arg0) {
        				// Get goal data
        				int homeGoals = Integer.parseInt(home.getText()) ;
        				int awayGoals = Integer.parseInt(away.getText()) ;
        				
        				// Get which teams were playing
        				Team homeTeam = homeList.getSelectedValue() ;
        				Team awayTeam = awayList.getSelectedValue() ;
        				
        				// Find each team objects and change their data
        				for (Team t : teamObjects) {
        					if (t == homeTeam) {
        						t.setGoalsPro(homeGoals) ; 
        						t.setGoalsCon(awayGoals) ;
        						t.incrementGamesPlayed() ;
        						t.updateGoalsDifference() ;
        						if (homeGoals > awayGoals) {t.addPoints(3);}
        						else if (homeGoals == awayGoals) {t.addPoints(1);}
        					}
        					else if (t == awayTeam) {
        						t.setGoalsPro(awayGoals) ; 
        						t.setGoalsCon(homeGoals) ;
        						t.incrementGamesPlayed() ;
        						t.updateGoalsDifference() ;
        						if (awayGoals > homeGoals) {t.addPoints(3);}
        						else if (homeGoals == awayGoals) {t.addPoints(1);}
        					}
        				}
        				matchWindow.setVisible(false);	// Close window when done
        				
        				// Update JTables in main window
        				scrollPane2.setViewportView ( new JTable (makeLeagueRows(), makeLeagueHeader())  ) ;
        				scrollPane.setViewportView  ( new JTable (makeRows(), makeHeader()) );
        			}
        		});
        		matchPanel.add(btnAddMatch);
        		
        		// Open window
        		matchWindow.setSize(800,600) ;
        		matchWindow.setVisible(true);
        	}
        	
        }
       
        
        
//		############################################
//		###############    MAIN    #################
//		############################################
        
        public static void main(String[] args) {
                reader() ;
                @SuppressWarnings("unused")
				PremiershipOO p = new PremiershipOO() ;
                save() ;
        }
}



/*
 * I don't need to open a new window. I could just redraw the current one. The code will be cleaner. (see solution EPLSystem)
 */

