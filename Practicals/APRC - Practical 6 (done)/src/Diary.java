import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Scanner;



public class Diary {
	// This is the location of my folder
	static File entries = new File ("entries") ;
	
	static boolean s = entries.isDirectory() ;
	static Calendar c = Calendar.getInstance() ;
		
	public static void runDiary() {
		
		// If there are no files in my folder, display error message
		if (entries.list().length == 0) { System.out.println("\nThere are no entries in the diary.\n") ; }
		
		// Allow user to input text
		Scanner sc = new Scanner(System.in) ;
		String text = "" ;
		System.out.print("Please write diary entry : ");
		text = sc.nextLine() ;
		sc.close() ;
		
		// Get current date and store it as a string for later use as a filename
		// If-else statement is there to enforce a format of 2013.03.02 (instead of 2013.3.2)
		String date = "" ;
		if (c.get(Calendar.DATE) < 10 ) { date = (c.get(Calendar.YEAR) + "") + "." + (c.get(Calendar.MONTH) + "") + ".0" + (c.get(Calendar.DATE) + "") ; }
		else { date = (c.get(Calendar.YEAR) + "") + "." + (c.get(Calendar.MONTH) + "") + "." + (c.get(Calendar.DATE) + "") ; }
		
		// Open writer
		File file = new File (entries + "/" + date + ".txt") ;
		BufferedWriter writer ;
		
		// If today's file already exists, append. Otherwise, create.
		if (file.exists()) { 
			try {
				writer = new BufferedWriter( new FileWriter(file, true) ) ;
				writer.append("\n\n** "+ c.get(Calendar.HOUR_OF_DAY) + "." + c.get(Calendar.MINUTE) + "\n" + text) ;
				writer.close() ;
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else {
			try {
				writer = new BufferedWriter(new FileWriter(file)) ;
				writer.write("** "+ c.get(Calendar.HOUR_OF_DAY) + "." + c.get(Calendar.MINUTE) + "\n" + text) ;
				writer.close() ;
				
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	
	public static void main (String[] arg0) {
		runDiary() ;
	}
}


/*
 * My implementation does not open files to read them, it just writes new entries to the diary.
 * I could have had a do-while loop to let the user enter multiple entries at one go.
 */




