
public class Team {
	
	private String name ;
	private int games_played, goals_pro, goals_con, points, goals_difference, rank ;
	
	public Team (String n, int gp, int gpro, int gcon, int p) {
		name = n ;
		games_played = gp ;
		goals_pro = gpro ;
		goals_con = gcon ;
		points = p ;
		goals_difference = goals_pro - goals_con ;
		rank = 0 ;
	}
	
	public String getName() {return name;}
	public int getGamesPlayed() {return games_played;}
	public int getGoalsPro() {return goals_pro;}
	public int getGoalsCon() {return goals_con;}
	public int getPoints() {return points;}
	public int getGoalsDifference() {return goals_difference;}
	public int getRank() {return rank;}
	
	public void incrementGamesPlayed() {games_played += 1 ;}
	public void setGoalsPro(int number) {goals_pro += number ;}
	public void setGoalsCon(int number) {goals_con -= number ;}
	public void updateGoalsDifference() {goals_difference = goals_pro - goals_con ;}
	public void setRank(int number) {rank = number ;} 
	public void addPoints(int number) {points += number ;}
	
	public String toString() {
		return name ;
	}

}
