import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
 
 
public class Numbers {
       
        static ArrayList<Integer> list1 = new ArrayList<Integer>() ;
        static ArrayList<Integer> list2 = new ArrayList<Integer>() ;
        static ArrayList<Integer> sorted = new ArrayList<Integer>() ;
        static int n, sum ;
        static String number ;
       
        // Reads the first numbers file
        public static void reader1() {
                try {
                        BufferedReader r1 = new BufferedReader (new FileReader("numbers1.txt")) ;		// Read the file
                       
                        Scanner sc1 = new Scanner(r1) ;													// Throw the reader in a scanner
                        while (sc1.hasNextLine()) {														// If there is another line,
                                number = sc1.nextLine() ;												// get the number and add it to the corresponding list
                                n = Integer.parseInt(number) ;
                                list1.add(n) ;
                        }
                        sc1.close() ;
                       
                } catch (FileNotFoundException e) { e.printStackTrace(); }								// Unless there is no such file
        }
       
        // Reads the second numbers file
        public static void reader2() {
                try {
                        BufferedReader r2 = new BufferedReader (new FileReader("numbers2.txt")) ;	// Read the file
                       
                        Scanner sc2 = new Scanner(r2).useDelimiter(" ") ;							// Throw the file in a scanner 
                        while (sc2.hasNext()) {														// There is only one line in this file, so if there is a next value,
                                n = Integer.parseInt(sc2.next()) ;									// parse it into an integer and add it to the corresponding list
                                list2.add(n) ;
                        }
                        
                } catch (FileNotFoundException e) { e.printStackTrace(); }							// Unless the file doesn't exist
        }
       
        // Returns the sum of all numbers in an arraylist of integers
        public static int sum(ArrayList<Integer> a) {
                sum = 0;
                for ( int i=0 ; i<a.size(); i++ ) {
                        sum += a.get(i) ;
                }
                return sum ;
        }
       
        // Returns the mean of all numbers in an arraylist of integers
        public static double avg(ArrayList<Integer> a) {
                return (double) sum(a) / a.size();
        }
       
        // Prints the largest and smallest values in an arraylist of integers
        public static void extremes(ArrayList<Integer> a) {
                sorted = new ArrayList<Integer> (a) ;
                Collections.sort(sorted) ;
                int max = sorted.get(sorted.size()-1) ;
                int min = sorted.get(0) ;
                System.out.println("The largest value is " + max) ;
                System.out.println("The smallest value is " + min) ;
        }
       
        // Compares two arraylists of integers, returns true if they are the same, false if not
        public static boolean isSame(ArrayList<Integer> a, ArrayList<Integer> b) {
                if (a.size() != b.size()) {return false ;}
                else {
                        for (int i=0 ; i<=a.size()-1 ; i++) {
                                if (a.get(i) != b.get(i)) {return false ;}
                        }
                }
                return true ;
        }
       
        // Writes the first new numbers file
        public static void writer(ArrayList<Integer> a, String filename) {
                try {
                        BufferedWriter writer = new BufferedWriter( new FileWriter(filename) ) ;	// Open a writer
                       
                        for ( int i=0 ; i<a.size()-1 ; i+=2 ) {
                                writer.write(a.get(i)+","+a.get(i+1)+"\n") ;						// Write the numbers in pairs on each line, separated by a comma 
                        }
                        writer.close() ;
                } 
                catch (IOException e) { e.printStackTrace(); }
               
               
        }
       
        public static void main(String[] args) {
                reader1();
                System.out.println(list1) ;
                reader2();
                System.out.println(list2) ;
                System.out.println(sum(list1)) ;
                System.out.println(avg(list1)) ;
                extremes(list1) ;
                System.out.println(isSame(list1,list2)) ;
                writer(list1, "newNumbers1.txt") ;
                writer(list2, "newNumbers2.txt") ;
        }
 
}