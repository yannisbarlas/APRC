import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class mp4player extends mp3player{
	
	protected int capacity ;
	protected JButton btnAddMp4 ;
	
	public mp4player (int c) {
		super();
		capacity = c ;
		mp4gui() ;
	}
	
	public void mp4gui() {
		JLabel lblMem = new JLabel ("Memory Left") ;
		lblMem.setBounds(450, 250, 100, 20);
		panel.add(lblMem) ;
		
		mem = new JLabel("" + memoryUsed) ;
		mem.setBounds(550, 250, 50, 20);
		panel.add(mem) ;
		
		makeAddMp4Button() ;
	}
	
	protected void makeAddMp4Button() {
    	btnAddMp4 = new JButton("Add Mp4");
    	btnAddMp4.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		// Creates new window of the class addMp3
        		@SuppressWarnings("unused")
				addMp4 am4 = new addMp4() ;
        	}
        });
    	btnAddMp4.setBounds(360, 183, 153, 29);
        panel.add(btnAddMp4);
    }
	
	@SuppressWarnings("serial")
	class addMp4 extends JFrame {
		JFrame addMp4Window ;
		JPanel addMp4Panel ;
		JTextField newMp4Artist, newMp4Name, newMp4Memory ;

    	public addMp4() {
    		// Window basics
    		addMp4Window = new JFrame("Edit song details") ;
    		addMp4Panel = new JPanel() ;
    		addMp4Window.setContentPane(addMp4Panel) ;
    		addMp4Window.getContentPane().setLayout(null);
    		
    		// Add labels and text fields that allow user to input song data
    		JLabel lblMp4ArtistName = new JLabel("Artist Name");
    		lblMp4ArtistName.setBounds(32, 74, 152, 16);
    		addMp4Window.getContentPane().add(lblMp4ArtistName);
    		
    		newMp4Artist = new JTextField();
    		newMp4Artist.setBounds(264, 68, 134, 28);
    		addMp4Window.getContentPane().add(newMp4Artist);
    		newMp4Artist.setColumns(10);
    		
    		JLabel lblMp4SongTitle = new JLabel("Song Title");
    		lblMp4SongTitle.setBounds(32, 114, 152, 16);
    		addMp4Window.getContentPane().add(lblMp4SongTitle);
    		
    		newMp4Name = new JTextField();
    		newMp4Name.setBounds(264, 108, 134, 28);
    		addMp4Window.getContentPane().add(newMp4Name);
    		newMp4Name.setColumns(10);
    		
    		JLabel lblMp4Memory = new JLabel("Memory");
    		lblMp4Memory.setBounds(32, 154, 152, 16);
    		addMp4Window.getContentPane().add(lblMp4Memory);
    		
    		newMp4Memory = new JTextField();
    		newMp4Memory.setBounds(264, 148, 134, 28);
    		addMp4Window.getContentPane().add(newMp4Memory);
    		newMp4Memory.setColumns(10);
    		
    		// Add button that takes the data and makes a song
    		JButton btnAddMp4 = new JButton("Add song!");
    		btnAddMp4.setBounds(145, 195, 151, 24);
    		btnAddMp4.addActionListener( new ActionListener() {
    			@SuppressWarnings("unchecked")
				@Override
    			public void actionPerformed(ActionEvent arg0) {
    				String amp4 = newMp4Artist.getText() ;			// Get user input
    				String tmp4 = newMp4Name.getText() ;
    				String mmp4 = newMp4Memory.getText();
    				
    				try { 
    					int mint = Integer.parseInt(mmp4) ;
    					
    					if (! amp4.equals("")) {						// Validate user input for both fields 
        					if (! tmp4.equals("")) {
        						mp4 m = new mp4(amp4,tmp4, mint) ;
        						model.addElement(m) ;				// Add new mp3 to list model and array list
        						allSongs.add(m) ;
        						if (memoryUsed >= mint) {
        							memoryUsed -= mint ;
        							mem.setText(""+memoryUsed);
        						}
        						else {
        							JOptionPane.showMessageDialog(searchWindow, "You've run out of memory!", "Memory shortage", JOptionPane.PLAIN_MESSAGE) ;
        						}
        					}
        				}
        				addMp4Window.setVisible(false);				// Automatically close new window when done
    				} 
    				catch ( NumberFormatException e ) { JOptionPane.showMessageDialog(searchWindow, "Please enter an integer at the memory field", "Not an integer", JOptionPane.PLAIN_MESSAGE) ; }
    			}
    		});
    		addMp4Window.getContentPane().add(btnAddMp4);
    		
    		// Open window
    		addMp4Window.setSize(450,300);
    		addMp4Window.setVisible(true) ;
    	}
	}
	
	
	
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		mp4player m = new mp4player(400) ;
	}

}
