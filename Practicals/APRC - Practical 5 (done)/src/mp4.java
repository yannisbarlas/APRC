
public class mp4 extends mp3 {
	
	int memory ;
	
	public mp4(String a, String t, int m) {
		super(a, t);
		memory = m ;
	}
	
	public int getMemory() { return memory ; } 

	public static void main(String[] args) {
		mp4 m = new mp4("a","t",3) ;
		System.out.println(m.getArtist()) ;
		System.out.println(m.getTitle()) ;
		System.out.println(m.getMemory()) ;
		System.out.println(m.toString()) ;
	}

}
