 
public class mp3 {
       
        String artist, title ;
        
        public mp3 (String a, String t) {
        	artist = a ;
        	title = t ;
        }
       
        // Getter functions
        public String getArtist() { return artist ; }
        public String getTitle() { return title ; }
        
        // Manipulate song info
        public void changeArtist(String s) { artist = s ; }
        public void changeTitle(String s) { title = s ; }
        
        // To string
        public String toString() { return artist + " - " + title ; }
       
 
}