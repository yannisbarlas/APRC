
import java.util.ArrayList;
 
public class playlist {
       
        String name ;
        ArrayList<String> songs ;
       
        public playlist (String n) {
        	name = n ;
        	songs = new ArrayList<String> () ;
        }
        
        // Getter functions
        public String getName() { return name ; }
        public ArrayList<String> getSongs() { return songs ; }
        
        // Rename playlist
        public void renamePlaylist(String s3) { name = s3 ; }
        
        // Add songs to / remove songs from playlist
        public void addMp3(mp3 m) { songs.add(m.getTitle()) ; }
        public void removeMp3(mp3 m) { songs.remove(m.getTitle()) ; }
        
        // To string
        public String toString() { return name ; }
       
}
