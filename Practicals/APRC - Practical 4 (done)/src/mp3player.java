import java.awt.Color;
//import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;



















//import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
 
public class mp3player {
       
		static ArrayList<playlist> playlists = new ArrayList<playlist> () ;
        static ArrayList<mp3> allSongs = new ArrayList<mp3> () ;
        List<String> searchList  = new ArrayList<String>() ;
        
        JFrame window, searchWindow, editWindow ;
        JPanel panel ;
        private JLabel lblPlaylistName, lblNewLabel;
        JTextField searchArtist, searchTitle, playlistName, newName, newArtist, newPlName ;
        private JTextField artist, title;
        @SuppressWarnings("rawtypes")
		JList list, playlistList ;
        @SuppressWarnings("rawtypes")
		public JList searchResults ;
        @SuppressWarnings("rawtypes")
		DefaultListModel model, searchListModel, plModel, plSongsModel ;
        private JButton btnCreatePlaylist, btnRenamePlaylist, btnDeletePlaylist, btnAddSongTo, btnDeleteSongFrom, btnAddSong, btnEditSongDetails ;
       
//      ##################################################################
//      ##########################   GUI   ###############################
//      ##################################################################
       
        private mp3player () {
                setUpGUI() ;
        }
       
        @SuppressWarnings({ "rawtypes", "unchecked" })
		public void setUpGUI () {
        	
        		// Window basics 
                window = new JFrame("mp3 player") ;
                window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
               
                panel = new JPanel() ;
                window.setContentPane(panel) ;
                panel.setLayout(null);
                
                // Songs label and list of mp3's 
                JLabel songs = new JLabel("Songs");
                songs.setBounds(12, 13, 70, 15);
                panel.add(songs);
                
                makeSongList() ;
                
                // Manipulate song buttons
                makeDeleteButton() ;
                makeEditSongButton() ;
                makeAddSongButton() ;
                
                // Song artist and title text fields                
                JLabel artist_label = new JLabel("Artist");
                artist_label.setBounds(360, 38, 70, 15);
                panel.add(artist_label);
               
                JLabel title_label = new JLabel("Title");
                title_label.setBounds(360, 65, 70, 15);
                panel.add(title_label);
               
                artist = new JTextField();
                artist.setBounds(442, 35, 114, 19);
                panel.add(artist);
                artist.setColumns(10);
               
                title = new JTextField();
                title.setBounds(442, 62, 114, 19);
                panel.add(title);
                title.setColumns(10);
                
                // Search button and list box of search results
                makeSearch() ;
                
                // Playlist list box
                makePlaylistBox() ;
                
                // Playlist name label & text field
                lblPlaylistName = new JLabel("Playlist Name");
                lblPlaylistName.setBounds(6, 425, 85, 16);
                panel.add(lblPlaylistName);
                
                playlistName = new JTextField();
                playlistName.setBounds(103, 419, 134, 28);
                panel.add(playlistName);
                playlistName.setColumns(10);
                
                // Manipulate playlist button
                makeCreatePlaylistButton() 			;
                makeRenamePlaylistButton() 			;
                makeDeletePlaylistButton() 			;
                makeAddSongToPlaylistButton() 		;
                makeDeleteSongFromPlaylistButton() 	;
                
                // Songs in playlist list box
                lblNewLabel = new JLabel("Playlist Songs");
                lblNewLabel.setBounds(569, 381, 107, 16);
                panel.add(lblNewLabel);
                
                plSongsModel = new DefaultListModel() ;
                JList playListSongs = new JList(plSongsModel);
                playListSongs.setBounds(559, 409, 264, 186);
                panel.add(playListSongs);

                // Make window
                window.setSize(1000,700) ;
                window.setVisible(true) ;
        }
        
//      #### GUI FUNCTIONS ####
        
        // Makes list box containing all mp3 songs
        @SuppressWarnings({ "rawtypes", "unchecked" })
		private void makeSongList() {
        	// Make default list model, add allSongs's elements to it and add model to jlist 
        	model = new DefaultListModel() ;
            for (int i=0 ; i<allSongs.size(); i++) { model.addElement(allSongs.get(i)); }
            list = new JList(model) ;
            list.setBounds(12, 37, 292, 149);
            
            // List selection listener, displaying song information in text fields
            ListSelectionListener listen = new ListSelectionListener() {
            	@Override
				public void valueChanged(ListSelectionEvent arg0) {
            		if (list.getSelectedValue() != null) {
						String selectedSong = (String) ((mp3) list.getSelectedValue()).getArtist() ;
						artist.setText(selectedSong);
						String selectedArtist = (String) ((mp3) list.getSelectedValue()).getTitle() ;
						title.setText(selectedArtist);
            		}
				}
            } ;
            list.addListSelectionListener(listen);
            
            // Add jlist to panel
            window.getContentPane().add(list) ;
        }
        
        // Makes button that deletes mp3s
        private void makeDeleteButton() {
        	JButton deleteSong = new JButton("Delete Song");
        	deleteSong.setBounds(360, 132, 153, 25);
            deleteSong.addActionListener( new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent arg0) {
            		// Look into each playlist and delete song that will be deleted from them
            		for ( playlist p : playlists) {
            			System.out.println(p);
            			System.out.println(p.getSongs()) ;
            			p.getSongs().remove( ( (mp3)list.getSelectedValue() ).getTitle() ) ;
            		}
            		
            		// Remove song from array list and from the model that goes into the corresponding jlist
            		allSongs.remove(list.getSelectedValue()) ;
            		int i = list.getSelectedIndex() ;
            		model.removeElementAt(i);
            		
            	}
            });
            
            // Add button to panel
            panel.add(deleteSong);
        }
        
        // Makes button that lets user edit song information
        private void makeEditSongButton() {
        	btnEditSongDetails = new JButton("Edit Song Details");
            btnEditSongDetails.setBounds(360, 157, 153, 29);
            btnEditSongDetails.addActionListener( new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent arg0) {
            		// Creates new window of the class EditMp3
            		@SuppressWarnings("unused")
					editMp3 e = new editMp3() ;
            	}
            });
            
            // Add button to panel
            panel.add(btnEditSongDetails);
        }
        
        // Makes button that lets user add a song to the library
        private void makeAddSongButton() {
        	btnAddSong = new JButton("Add song");
            btnAddSong.addActionListener(new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent e) {
            		// Creates new window of the class addMp3
            		@SuppressWarnings("unused")
					addMp3 am = new addMp3() ;
            	}
            });
            btnAddSong.setBounds(360, 103, 153, 29);
            panel.add(btnAddSong);
        }
        
        // Creates search button and black list box containing search results
        @SuppressWarnings({ "rawtypes", "unchecked" })
		private void makeSearch() {
        	JButton search = new JButton("Search");
            search.setBounds(832, 65, 117, 29);
            search.addActionListener( new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent arg0) {
            		// Opens new window of the class search
            		@SuppressWarnings("unused")
					search sw = new search() ;
            	}
            });
            panel.add(search);
            
            // Create default list model and add it to a jlist, then add the jlist to the panel 
            searchListModel = new DefaultListModel() ;
            searchResults = new JList(searchListModel);
            searchResults.setForeground(Color.WHITE);
            searchResults.setBackground(Color.BLACK);
            searchResults.setBounds(641, 108, 308, 197);
            panel.add(searchResults);
        }
        
        // Makes list box containing the playlists
        @SuppressWarnings({ "rawtypes", "unchecked" })
		private void makePlaylistBox () {
        	// Create label for list box
        	JLabel lblPlaylists = new JLabel("Playlists");
            lblPlaylists.setBounds(249, 381, 61, 16);
            panel.add(lblPlaylists);
            
            // Create default list model for list box, add all playlist objects to it, then add model to a jlist
            plModel = new DefaultListModel() ;
            for ( int i=0 ; i<playlists.size() ; i++ ) { plModel.addElement(playlists.get(i)) ; }
            playlistList = new JList(plModel);
            
            // List selection listener to populate corresponding text field, as well as list box containing songs that belong to the playlist
            ListSelectionListener currentPlaylist = new ListSelectionListener() {
            	@Override
            	public void valueChanged (ListSelectionEvent arg0) {
                		playlist p = (playlist) playlistList.getSelectedValue() ;
                		if (p != null) { 
                			playlistName.setText(p.getName()) ;								// set textfield to display playlist's name
                			plSongsModel.clear() ;											// empty list box before repopulating it
                			ArrayList<String> s = new ArrayList<String>(p.getSongs()) ;		// populate list box with playlist's songs
                    		for ( int i=0 ; i<s.size(); i++ ) {
                    			plSongsModel.addElement(s.get(i)) ;
                    		}
               			}
            	}
            };
            
            playlistList.addListSelectionListener(currentPlaylist) ;		// Add list selection listener to list
            playlistList.setBounds(249, 409, 264, 186);
            panel.add(playlistList);										// Add list to panel
        }
        
        // Makes button that creates a new playlist
        private void makeCreatePlaylistButton() {
        	btnCreatePlaylist = new JButton("Create playlist");
            btnCreatePlaylist.setBounds(53, 477, 141, 29);
            btnCreatePlaylist.addActionListener( new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent arg0) {
            		// Opens new window of the class createPlaylist
            		@SuppressWarnings("unused")
					createPlaylist cp = new createPlaylist() ;
            	}
            });
            panel.add(btnCreatePlaylist);
        }
        
        // Makes button that lets user rename a playlist
        private void makeRenamePlaylistButton() {
        	btnRenamePlaylist = new JButton("Rename playlist");
            btnRenamePlaylist.setBounds(53, 507, 141, 29);
            btnRenamePlaylist.addActionListener( new ActionListener() {
            	@Override
            	public void actionPerformed (ActionEvent arg0) {
            		// Opens new window of the class renamePlaylist
            		@SuppressWarnings("unused")
					renamePlaylist rnpl = new renamePlaylist() ;
            	}
            });
            panel.add(btnRenamePlaylist);
        }
        
        // Makes button that lets user rename a playlist
        private void makeDeletePlaylistButton() {
        	btnDeletePlaylist = new JButton("Delete Playlist");
            btnDeletePlaylist.setBounds(53, 537, 141, 29);
            btnDeletePlaylist.addActionListener( new ActionListener() {
            	@Override
            	public void actionPerformed (ActionEvent arg0) {
            		// Remove selected playlist from the playlist list box
            		plModel.removeElement(playlistList.getSelectedValue()) ;
            	}
            });
            panel.add(btnDeletePlaylist);
        }
        
        // Makes button that lets user add a song to the selected playlist
        private void makeAddSongToPlaylistButton() {
        	btnAddSongTo = new JButton("Add song to playlist");
            btnAddSongTo.setBounds(12, 229, 203, 29);
            btnAddSongTo.addActionListener( new ActionListener () {
            	@Override
            	public void actionPerformed (ActionEvent arg0) {
            		// Get selected song from song list box and add it to selected playlist in playlist list box
            		( (playlist) playlistList.getSelectedValue()).addMp3( (mp3) list.getSelectedValue()) ;
            	}
            });
            panel.add(btnAddSongTo);
        }
        
        // Makes button that lets user remove a song from a playlist
        private void makeDeleteSongFromPlaylistButton() {
        	btnDeleteSongFrom = new JButton("Delete song from playlist");
            btnDeleteSongFrom.addActionListener(new ActionListener() {
            	@Override
            	public void actionPerformed(ActionEvent e) {
            		// Get selected song from song list box and remove it from selected playlist
            		( (playlist) playlistList.getSelectedValue()).removeMp3( (mp3) list.getSelectedValue()) ;
            	}
            });
            btnDeleteSongFrom.setBounds(12, 260, 203, 29);
            panel.add(btnDeleteSongFrom);
        }
        
//      #### GUI NEW WINDOWS ####
        
        // New window that opens with search button
        @SuppressWarnings("serial")
		class search extends JFrame {
        	public search() {
        		// Window basics 
        		searchWindow = new JFrame("Search") ;
            	JPanel searchPanel = new JPanel() ;
            	searchWindow.setContentPane(searchPanel) ;
            	
            	// Make labels and text fields for user input
            	JLabel sArtist = new JLabel("Enter artist name") ;
            	sArtist.setBounds(77, 75, 107, 16);
            	searchArtist = new JTextField() ;
            	searchArtist.setBounds(220, 69, 118, 28);
            	
            	JLabel sTitle = new JLabel("Enter song title") ;
            	sTitle.setBounds(77, 103, 96, 16);
            	searchTitle = new JTextField() ;
            	searchTitle.setBounds(220, 97, 118, 28);
            	
            	// Add labels and text fields to panel
            	searchPanel.setLayout(null);
            	searchPanel.add(sArtist) ;
            	searchPanel.add(searchArtist) ;
            	searchPanel.add(sTitle) ;
            	searchPanel.add(searchTitle) ;
            	
            	// Make button that searches for user input matches
            	JButton searchButton = new JButton("Search!");
            	searchButton.setBounds(141, 176, 117, 29);
            	searchButton.addActionListener( new ActionListener() {
            		@SuppressWarnings("unchecked")
					@Override
            		public void actionPerformed(ActionEvent arg0) {
            			searchListModel.clear() ;															// Clear list from any previous search results
            			String a = searchArtist.getText() ;													// Get artist name from user input
            			String t = searchTitle.getText() ;													// Get song title from user input
            			for ( int i=0 ; i<allSongs.size(); i++ ) {
            				mp3 song = allSongs.get(i) ;													// In each song we have,
            				if ( song.getArtist().equals(a) || song.getTitle().equals(t) ) {				// if user input matches song info,
            					searchListModel.addElement(song.getArtist() + " - " + song.getTitle()) ;	// add song to search results
            				}
            			}
            			
            			// If no songs match user search, then display appropriate message
            			if (searchListModel.size() == 0) { JOptionPane.showMessageDialog(searchWindow, "No songs match your search criteria!", "No results", JOptionPane.PLAIN_MESSAGE); }
        				
            			// Automatically close window after search is complete
            			searchWindow.setVisible(false) ;
            		}
            	});
            	searchPanel.add(searchButton);
            	
            	// Open window
            	searchWindow.setSize(400,300) ;
            	searchWindow.setVisible(true);
        	}
        }
        
        // New window that opens with edit song information button
        @SuppressWarnings("serial")
		class editMp3 extends JFrame {
        	public editMp3() {
        		// Window basics
        		editWindow = new JFrame("Edit song details") ;
        		JPanel editPanel = new JPanel() ;
        		editWindow.setContentPane(editPanel) ;
        		editWindow.getContentPane().setLayout(null);
        		
        		// Add labels and text fields that user can use to change song information
        		JLabel lblChangeArtistName = new JLabel("Change Artist Name");
        		lblChangeArtistName.setBounds(32, 74, 152, 16);
        		editWindow.getContentPane().add(lblChangeArtistName);
        		
        		newArtist = new JTextField();
        		newArtist.setBounds(264, 68, 134, 28);
        		editWindow.getContentPane().add(newArtist);
        		newArtist.setColumns(10);
        		
        		JLabel lblChangeSongTitle = new JLabel("Change Song Title");
        		lblChangeSongTitle.setBounds(32, 119, 152, 16);
        		editWindow.getContentPane().add(lblChangeSongTitle);
        		
        		newName = new JTextField();
        		newName.setBounds(264, 113, 134, 28);
        		editWindow.getContentPane().add(newName);
        		newName.setColumns(10);
        		
        		// Add a button that performs the changes the user has chosen
        		JButton btnApplyChanges = new JButton("Apply Changes");
        		btnApplyChanges.setBounds(145, 182, 151, 24);
        		btnApplyChanges.addActionListener( new ActionListener() {
        			@Override
        			public void actionPerformed(ActionEvent arg0) {
        				mp3 m = (mp3) list.getSelectedValue() ;			// Get selected song
        				String a = newArtist.getText() ;				// Get user input
        				String t = newName.getText() ;
        				if (! a.equals("")) { m.changeArtist(a) ; }		// If input is valid, change song information
        				if (! t.equals("")) { m.changeTitle(t) ;}
        				editWindow.setVisible(false);					// Automatically close window once changes have been applied
        			}
        		});
        		editWindow.getContentPane().add(btnApplyChanges);
        		
        		// Open window
        		editWindow.setSize(450,300);
        		editWindow.setVisible(true) ;
        	}
        	
        	
        }
        
        // New window that opens with add song button
        @SuppressWarnings("serial")
		class addMp3 extends JFrame {
        			
        			JFrame addWindow ;
        			JPanel addPanel ;
        			JTextField newArtist, newName ;
        	
                	public addMp3() {
                		// Window basics
                		addWindow = new JFrame("Edit song details") ;
                		addPanel = new JPanel() ;
                		addWindow.setContentPane(addPanel) ;
                		addWindow.getContentPane().setLayout(null);
                		
                		// Add labels and text fields that allow user to input song data
                		JLabel lblArtistName = new JLabel("Artist Name");
                		lblArtistName.setBounds(32, 74, 152, 16);
                		addWindow.getContentPane().add(lblArtistName);
                		
                		newArtist = new JTextField();
                		newArtist.setBounds(264, 68, 134, 28);
                		addWindow.getContentPane().add(newArtist);
                		newArtist.setColumns(10);
                		
                		JLabel lblSongTitle = new JLabel("Song Title");
                		lblSongTitle.setBounds(32, 119, 152, 16);
                		addWindow.getContentPane().add(lblSongTitle);
                		
                		newName = new JTextField();
                		newName.setBounds(264, 113, 134, 28);
                		addWindow.getContentPane().add(newName);
                		newName.setColumns(10);
                		
                		// Add button that takes the data and makes a song
                		JButton btnAddSong = new JButton("Add song!");
                		btnAddSong.setBounds(145, 182, 151, 24);
                		btnAddSong.addActionListener( new ActionListener() {
                			@SuppressWarnings("unchecked")
							@Override
                			public void actionPerformed(ActionEvent arg0) {
                				String a = newArtist.getText() ;			// Get user input
                				String t = newName.getText() ;
                				if (! a.equals("")) {						// Validate user input for both fields 
                					if (! t.equals("")) {
                						mp3 m = new mp3(a,t) ;
                						model.addElement(m) ;				// Add new mp3 to list model and array list
                						allSongs.add(m) ;
                					}
                				}
                				addWindow.setVisible(false);				// Automatically close new window when done
                			}
                		});
                		addWindow.getContentPane().add(btnAddSong);
                		
                		// Open window
                		addWindow.setSize(450,300);
                		addWindow.setVisible(true) ;
                	}
                	
                	
                }
        
        // New window that opens with create playlist button
        @SuppressWarnings("serial")
		class createPlaylist extends JFrame{
        	
        	JFrame cplWindow ;
        	JPanel cplPanel ;
//        	private JTextField textField;
        	
        	public createPlaylist() {
        		//Window basics
        		cplWindow = new JFrame ("Create new playlist") ;
        		cplPanel = new JPanel() ;
        		cplWindow.setContentPane(cplPanel);
        		cplWindow.getContentPane().setLayout(null);
        		
        		// Add label and text field, allowing for user input
        		JLabel lblPleaseEnterA = new JLabel("Please enter a name for the new playlist");
        		lblPleaseEnterA.setBounds(96, 74, 285, 16);
        		cplWindow.getContentPane().add(lblPleaseEnterA);
        		
        		newPlName = new JTextField();
        		newPlName.setBounds(153, 120, 134, 28);
        		cplWindow.getContentPane().add(newPlName);
        		newPlName.setColumns(10);
        		
        		// Add button to create new playlist using user input 
        		JButton btnCreate = new JButton("Create");
        		btnCreate.setBounds(158, 181, 117, 29);
        		btnCreate.addActionListener ( new ActionListener () {
        			@SuppressWarnings("unchecked")
					@Override
        			public void actionPerformed (ActionEvent arg0) {
        				String n = newPlName.getText() ;			// Get user input
        				if (! n.equals("")) { 						// Make sure user input is valid
        					plModel.addElement(new playlist(n)) ;	// Add playlist to corresponding list box
        				}
        				cplWindow.setVisible(false);				// Automaticall close list box when done
        			}
        		});
        		cplWindow.getContentPane().add(btnCreate);
        		
        		// Open window
        		cplWindow.setSize(450,300) ;
        		cplWindow.setVisible(true) ;
        	}
        }
        
        // New window that opens with rename playlist button
        @SuppressWarnings("serial")
		class renamePlaylist extends JFrame {
        	
        	JFrame renamePlWindow ;
        	JPanel renamePlPanel ;
        	private JTextField textField;
        	
        	public renamePlaylist() {
        		// Window basics
        		renamePlWindow = new JFrame("Rename playlist") ;
        		renamePlPanel = new JPanel() ;
        		renamePlWindow.setContentPane(renamePlPanel);
        		renamePlWindow.getContentPane().setLayout(null);
        		
        		// Add label and text field that allow for user input
        		JLabel lblPleaseEnterPlaylists = new JLabel("Please enter playlist's new name");
        		lblPleaseEnterPlaylists.setBounds(109, 87, 221, 16);
        		renamePlWindow.getContentPane().add(lblPleaseEnterPlaylists);
        		
        		textField = new JTextField();
        		textField.setBounds(150, 115, 134, 28);
        		renamePlWindow.getContentPane().add(textField);
        		textField.setColumns(10);
        		
        		// Add button that renames playlist with user input
        		JButton btnRename = new JButton("Rename");
        		btnRename.setBounds(160, 155, 117, 29);
        		btnRename.addActionListener( new ActionListener() {
        			@Override
        			public void actionPerformed (ActionEvent arg0) {
        				String rename = textField.getText() ;								// Get user input
        				playlist chosenP = (playlist) playlistList.getSelectedValue() ;		// Get selected playlist
        				if (! rename.equals("")) { chosenP.renamePlaylist(rename); }		// Rename playlist if user input is valid
        				renamePlWindow.setVisible(false);									// Automatically close window when done
        			}
        		});
        		renamePlWindow.getContentPane().add(btnRename);
        		
        		// Open window
        		renamePlWindow.setSize(450,300) ;
        		renamePlWindow.setVisible(true);
        		
        	}
        }
        
//      ##################################################################
//      ##########################  Main  ################################
//      ##################################################################
       
        public static void main(String[] args) {
                playlist p1 = new playlist("new one") ;
                playlists.add(p1) ;
                playlist p2 = new playlist("another one") ;
                playlists.add(p2) ;
                mp3 m1 = new mp3("a1", "t1") ;
                mp3 m2 = new mp3("a2", "t2") ;
                allSongs.add(m1) ;
                allSongs.add(m2) ;
                p1.addMp3(m1) ;
                p1.addMp3(m2);
               
                @SuppressWarnings("unused")
				mp3player play = new mp3player() ;
        }
}


/*
 * Could have more separate and organised methods for the gui 
 */


//  ALTERNATIVE WAY OF DISPLAYING MP3 OBJECTS IN CORRECT FORMAT USING CELL RENDERER (LINE 130) 

//list.setCellRenderer(new DefaultListCellRenderer() {
//@Override
//public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
//	Component display = super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus) ;
//	((JLabel) display).setText(((mp3) value).getArtist() + " - " + ((mp3) value).getTitle());
//	return display ;
//}
//});