import java.awt.event.* ;

import javax.swing.* ;

public class gui_vending {
	
	static int left=500, extra, change=0, total ;
	static String receipt = "\nITEM\tCOST\tTotal\n" ;
	static String lowest ;
	private static JLabel money ;
	private static JTextArea recpt ;
	private static JFrame window ;
	static JLabel chocoStock,colaStock,crispsStock,currypotStock ;
	static JLabel fiveP, twoP, oneP, fiftyp, twentyp, tenp, fivep, twop, onep ;
	
	// Object instances are with a capital c and labels without one
	static VendingItem Chocolate = new VendingItem("Chocolate", 57, 5) ;
    static VendingItem Cola = new VendingItem("Cola", 71, 3) ;
    static VendingItem Crisps = new VendingItem("Crisps", 34, 4) ;
    static VendingItem Currypot = new VendingItem("Curry pot", 149, 2) ;

	// Call setUpGUI()
	private gui_vending() {
		setUpGUI() ;
	}
	
//	################################################
//	################	GUI		####################
//	################################################
	public void setUpGUI() {
		// Set window basics
		window = new JFrame("Vending Machine");
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel() ;
		window.setContentPane(panel);
		
		// Create chocolate line
		JLabel chocolate = new JLabel("1. Chocolate") ;
		chocolate.setBounds(5, 11, 79, 16);
		JLabel chocoPrice = new JLabel("Price: �0.57") ;
		chocoPrice.setBounds(96, 11, 74, 16);
		chocoStock = new JLabel("Stock left: " + Chocolate.getStock()) ;
		chocoStock.setBounds(196, 11, 76, 16);
		JButton chocoBuy = new JButton("Buy item") ;
		chocoBuy.setBounds(284, 6, 98, 29);
		panel.setLayout(null);
		window.getContentPane().add(chocolate) ;
		window.getContentPane().add(chocoPrice) ;
		window.getContentPane().add(chocoStock) ;
		window.getContentPane().add(chocoBuy) ;
		chocoBuy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent arg0 ) {
				buyItem(Chocolate) ;
				chocoStock.setText( "Stock left: " + Chocolate.getStock());
			}
		});
		
		// Create cola line
		JLabel cola = new JLabel("2. Cola") ;
		cola.setBounds(5, 39, 44, 16);
		JLabel colaPrice = new JLabel("Price: �0.71") ;
		colaPrice.setBounds(96, 39, 74, 16);
		colaStock = new JLabel("Stock left: " + Cola.getStock()) ;
		colaStock.setBounds(196, 39, 76, 16);
		JButton colaBuy = new JButton("Buy item") ;
		colaBuy.setBounds(284, 34, 98, 29);
		window.getContentPane().add(cola) ;
		window.getContentPane().add(colaPrice) ;
		window.getContentPane().add(colaStock) ;
		window.getContentPane().add(colaBuy) ;
		colaBuy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent arg0 ) {
				buyItem(Cola) ;
				colaStock.setText( "Stock left: " + Cola.getStock());
			}
		});
		
		// Create crisps line
		JLabel crisps = new JLabel("3. Crisps") ;
		crisps.setBounds(5, 67, 56, 16);
		JLabel crispsPrice = new JLabel("Price: �0.34") ;
		crispsPrice.setBounds(96, 67, 74, 16);
		crispsStock = new JLabel("Stock left: " + Crisps.getStock()) ;
		crispsStock.setBounds(196, 67, 76, 16);
		JButton crispsBuy = new JButton("Buy item") ;
		crispsBuy.setBounds(284, 62, 98, 29);
		window.getContentPane().add(crisps) ;
		window.getContentPane().add(crispsPrice) ;
		window.getContentPane().add(crispsStock) ;
		window.getContentPane().add(crispsBuy) ;
		crispsBuy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent arg0 ) {
				buyItem(Crisps) ;
				crispsStock.setText( "Stock left: " + Crisps.getStock());
			}
		});
		
		// Create curry pot line
		JLabel currypot = new JLabel("4. Curry Pot") ;
		currypot.setBounds(6, 97, 74, 16);
		JLabel currypotPrice = new JLabel("Price: �1.49") ;
		currypotPrice.setBounds(96, 97, 74, 16);
		currypotStock = new JLabel("Stock left: " + Currypot.getStock()) ;
		currypotStock.setBounds(196, 97, 76, 16);
		JButton currypotBuy = new JButton("Buy item") ;
		currypotBuy.setBounds(284, 92, 98, 29);
		window.getContentPane().add(currypot) ;
		window.getContentPane().add(currypotPrice) ;
		window.getContentPane().add(currypotStock) ;
		window.getContentPane().add(currypotBuy) ;
		currypotBuy.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed( ActionEvent arg0 ) {
				buyItem(Currypot) ;
				currypotStock.setText( "Stock left: " + Currypot.getStock());
			}
		});
		
		// Create insert pound button and functionality
		JButton insert = new JButton("Insert pound") ;
		insert.setBounds(222, 147, 124, 29);
		insert.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				insertPound() ;
			}
		});
		window.getContentPane().add(insert) ;
		
		// Create money left label
		money = new JLabel("Money left: �" + left/100 + "." + left%100) ;
		money.setBounds(53, 152, 117, 16);
		window.getContentPane().add(money) ;
		
		// Create complete sale button
		JButton complete = new JButton("Complete Sale") ;
		complete.setBounds(133, 194, 133, 29);
		complete.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				completeSale() ;
			}
		});
		window.getContentPane().add(complete) ;
		
		// Create cancel button and functionality
		JButton cancel = new JButton("Cancel");
		cancel.setBounds(143, 223, 117, 29);
		cancel.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				noSale() ;
			}
		});
		panel.add(cancel);
		
		// Create text area for receipt and no sale messages
		recpt = new JTextArea();
		recpt.setBounds(53, 267, 293, 562);
		panel.add(recpt);
		
		// Show window
		window.setSize(400,1000) ;
		window.setVisible(true);
	}
	
	
//	################################################
//	##############     METHODS     #################
//	################################################	
	
	// Lets user increase his money by one pound
	private static void insertPound() {
		extra += 100 ;													// keep track of inserted pounds, in case the user hits cancel
		left += 100 ;													// increase user's money
		money.setText("Money left: �" + left/100 + "." + left%100) ;	// update money label
	}
	
	// Checks if there is enough money and stock for the purchase and buys item
	private static void buyItem(VendingItem v) {
        if (left > v.getItemPrice()) {
                if (v.stockLeft()) {
                        v.buyItem() ;													// method from vendingItem object
                        left -= v.getItemPrice() ;										// reduce money left by item's price
                        money.setText("Money left: �" + left/100 + "." + left%100) ;	// update money label
                        addToReceipt(v.getItemName(), v.getItemPrice()) ;				// add data to receipt
                }
                else { JOptionPane.showMessageDialog(window, "Item out of stock!", "Out of stock", JOptionPane.PLAIN_MESSAGE); }
        }
        else { JOptionPane.showMessageDialog(window, "You don't have enough money to purchase this item!", "Insufficient amount of money", JOptionPane.PLAIN_MESSAGE);}
    }
	
	// Adds bought items to our receipt, so that it can be printed later
	private static void addToReceipt(String item, int price) {
        total += price ;
        receipt += "\n" +item + "\t" + price/100 + "." + price%100 + "\t"  + total/100 + "." + total%100 ;
	}
	
	// Calculates change and prints out receipt and the change breakdown
	private static void displayReceipt() {
        getLowestCoins(left) ;
        recpt.setText(receipt + lowest) ;
	}
	
	// Figures out the change breakdown
	private static void getLowestCoins(int c) {
        // Calculate change for each coin / note
		int five_pounds = c / 500 ;
        if (five_pounds != 0) {c = c - five_pounds*500 ;}
        int two_pounds = c / 200 ;
        if (two_pounds != 0) {c = c - two_pounds*200 ;}
        int pound  = c / 100 ;
        if (pound != 0) {c = c - pound*100 ;}
        int fifty  = c / 50  ;
        if (fifty != 0) {c = c - fifty*50 ;}
        int twenty = c / 20  ;
        if (twenty != 0) {c = c - twenty*20 ;}
        int ten    = c / 10  ;
        if (ten != 0) {c = c - ten*10 ;}
        int five   = c / 5   ;
        if (five != 0) {c = c - five*5 ;}
        int two    = c / 2   ;
        if (two != 0) {c = c - two*2 ;}
        int one    = c / 1   ;
        
        // Add all of that to the string lowest
        lowest = "\n\nChange due: �" + left/100 + "." + left%100 + "\n" ;
        lowest += "\n�5:\t" + five_pounds ;
        lowest += "\n�2:\t" + two_pounds ;
        lowest += "\n�1:\t" + pound ;
        lowest += "\n50p:\t" + fifty ;
        lowest += "\n20p:\t" + twenty ;
        lowest += "\n10p:\t" + ten ;
        lowest += "\n5p:\t" + five ;
        lowest += "\n2p:\t" + two ;
        lowest += "\n1p:\t" + one ;
	}
	
	// Maybe redundant
	private static void completeSale() {
	        displayReceipt() ;
	}
	
	// Lets the user cancel the whole thing
	private static void noSale() {
	        left = 500 + extra ;	// Remember to keep track of inserted pounds as well.
	        getLowestCoins(left) ;
	        recpt.setText("You didn't buy anything." + lowest);
	        
	        // Reset stock to original value for each item
	        Chocolate.resetStock()	;	chocoStock.setText("Stock left: " + Chocolate.getStock()) 	;
	        Cola.resetStock() 		;	colaStock.setText("Stock left: " + Cola.getStock()) 		;
	        Crisps.resetStock() 	;	crispsStock.setText("Stock left: " + Crisps.getStock()) 	;
	        Currypot.resetStock() 	;	currypotStock.setText("Stock left: " + Currypot.getStock()) ;
	}
	
	
//	################################################
//	################	MAIN	####################
//	################################################
	@SuppressWarnings("unused")
	public static void main (String[] args) {
		gui_vending vending = new gui_vending() ;
	}
}


/*
 * I could have broken down set up gui into several smaller methods. It's hard to read as it is.
 */