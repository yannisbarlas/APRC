import javax.swing.* ;

//import java.awt.* ;
import java.awt.event.* ;


public class gui_atm {
	
	private int blnc = 500 ;
	private JFrame window ;
	private JTextField deposit, withdraw ;
	private JLabel balance ;
	private int d, w ;
	
	private gui_atm() {
		setUpGUI() ;
	}
	
	public void setUpGUI() {
		// Set window basics
		window = new JFrame("ATM") ;
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel panel = new JPanel() ;
		window.setContentPane(panel) ;
		panel.setLayout(null);
		
		// Create deposit button and text field
		JButton dep = new JButton("Deposit amount") ;
		dep.setBounds(96, 19, 154, 29);
		window.getContentPane().add(dep) ;
		deposit = new JTextField("") ;
		deposit.setBounds(262, 18, 41, 28);
		window.getContentPane().add(deposit) ;
		
		// Create withdraw button and text field
		JButton withd = new JButton("Withdraw amount") ;
		withd.setBounds(96, 59, 154, 29);
		window.getContentPane().add(withd) ;
		withdraw = new JTextField("") ;
		withdraw.setBounds(262, 58, 41, 28);
		window.getContentPane().add(withdraw) ;
		
		// Create balance label and value
		balance = new JLabel("Balance: " + blnc) ;
		balance.setBounds(132, 115, 79, 16);
		window.getContentPane().add(balance) ;
		
		// Create deposit button functionality
		dep.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try {
					d = Integer.parseInt(deposit.getText()) ;
					if (100 <= d && d <= 500) { 
						blnc += d ;
						balance.setText("Balance: " + blnc) ;
						deposit.setText("");
					}
					else { JOptionPane.showMessageDialog(window, "The amount you deposit must be at least 100 pounds and no more than 500 pounds.", "Error in input", JOptionPane.PLAIN_MESSAGE) ; }
				}
				catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(window, "Please enter an integer!", "Error in input", JOptionPane.PLAIN_MESSAGE);
					deposit.setText("");
				}
			}
		});
		
		// Create withdraw button functionality
		withd.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				try { 
					w = Integer.parseInt(withdraw.getText()) ;
					if (w<10 | w>250) {JOptionPane.showMessageDialog(window, "The amount you withdraw must be at least 10 pounds and no more than 250 pounds!", "Error in Input", JOptionPane.PLAIN_MESSAGE);}
					else if (w > blnc) { JOptionPane.showMessageDialog(window, "The amount you withdraw cannot be larger than your current balance!", "Error in Input", JOptionPane.PLAIN_MESSAGE); }
					else {
						blnc -= w ;
						balance.setText("Balance: " + blnc);
						withdraw.setText("");
					}
				}
				catch (NumberFormatException e) { 
					JOptionPane.showMessageDialog(window, "Please enter an integer!", "Error in input", JOptionPane.PLAIN_MESSAGE);
					withdraw.setText("");
				}
				
			}
		});
		
		// Create exit button and functionality
		JButton exit = new JButton("Exit") ;
		exit.setBounds(262, 188, 75, 29);
		exit.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				window.setVisible(false) ;
			}
		});
		window.getContentPane().add(exit) ;
		
		// Show window
		window.setSize(400,300);
		window.setVisible(true);
	}
	
		
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		gui_atm atm = new gui_atm() ; 
	}

}


/*
 * This program might work correctly, but I should have created a separate class for accounts.
 * It works, but it sure ain't object-oriented...
 */
