 
public class VendingItem {
 
        private String name ;
        private int price, stock ;
        private int STOCK ;			// keeps original value of stock, in order to reset it if user hits cancel button
 
        public VendingItem(String n, int p, int s) {
                name = n ;
                price = p;
                stock = s ;
                STOCK = s ;
        }
       
        public String getItemName() {
                return this.name ;
        }
       
        public int getItemPrice () {
                return this.price ;
        }
        
        public int getStock() {
        	return this.stock ;
        }
        
        public void resetStock() {
        	this.stock = STOCK ;
        }
        
        // Returns true if there is any stock left.
        public boolean stockLeft() {
                if (stock>0) {return true ;}
                else {return false ;}
        }
        
        public void buyItem() {
                this.stock -= 1 ;
        }
        
        // Probably unnecessary.
        public String toString() {
                return this.getItemName() + "\tprice: " + this.getItemPrice()/100 + "." + this.getItemPrice()%100 + "\tstock = " + this.stock ;
        }
}