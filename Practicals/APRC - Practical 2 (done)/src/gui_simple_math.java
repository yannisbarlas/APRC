import javax.swing.JFrame ;
import javax.swing.JOptionPane;
import javax.swing.JPanel ;
import javax.swing.JTextField ;
import javax.swing.JLabel ;
import javax.swing.JComboBox ;
import javax.swing.JButton ;

import java.awt.event.ActionEvent ;
import java.awt.event.ActionListener ;


public class gui_simple_math {
	
	private JLabel label, r ;
	private JComboBox operators ;
	private String choice ;
	private JTextField text1, text2 ;
	private JFrame window ;
	private String result ;
	private int t1, t2 ;
	
	// I call this one in main.
	private gui_simple_math() {
		setUpGUI () ;
	}
	
	// I call this one in gui_simple_math
	public void setUpGUI() {
		// Basics
		window = new JFrame("Simple Math") ;
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE) ;
		
		JPanel panel = new JPanel() ;
		window.setContentPane(panel) ;
		panel.setLayout(null);
		
		// Create label and two input fields
		label = new JLabel("Please enter two integers:") ;
		label.setBounds(13, 11, 161, 16);
		window.getContentPane().add(label) ;
		
		text1 = new JTextField("") ;
		text1.setBounds(227, 5, 50, 28);
		window.getContentPane().add(text1) ;
		text2 = new JTextField("") ;
		text2.setBounds(297, 5, 49, 28);
		window.getContentPane().add(text2) ;
		
		// Create combo box with all calculation options
		String[] operatorStrings = {"sum", "subtraction", "multiplication", "integer division", "real division", "modulo"} ;
		operators = new JComboBox(operatorStrings) ;
		operators.setBounds(123, 58, 154, 27);
		operators.addActionListener ( new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent arg0) {
				choice = (String) operators.getSelectedItem() ;
			}
			
		}) ;
		window.getContentPane().add(operators) ;
		
		// Create calculate button and functionality
		JButton button = new JButton("Calculate!") ;
		button.setBounds(143, 97, 106, 29);
		button.addActionListener ( new ActionListener() {
			@Override
			public void actionPerformed (ActionEvent arg0) {
				try {
					t1 = Integer.parseInt(text1.getText()) ;
					t2 = Integer.parseInt(text2.getText()) ;
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(window, "Please enter an integer!", "Error in input", JOptionPane.PLAIN_MESSAGE);
				}
				
				if (choice == "sum") { result = "The sum of these two integers is " + (t1 + t2) + "." ;}
				else if (choice == "subtraction") { result = "The difference of these two integers is " + (t1 - t2) + " ." ;}
				else {
					if (choice == "multiplication") { result = "The product of these two integers is " + (t1 * t2) + " ." ; }
					else if (choice == "integer division") {
						if (t2 == 0) { JOptionPane.showMessageDialog(window, "Cannot divide by zero!", "Zero division error", JOptionPane.PLAIN_MESSAGE); }
						else { result = "The quotient of these two integers is " + (t1 / t2) + " ." ; }
						}
					else {
						if (choice == "real division") { 
							if (t2 == 0) { JOptionPane.showMessageDialog(window, "Cannot divide by zero!", "Zero division error", JOptionPane.PLAIN_MESSAGE); }
							else {result = "The quotient of these two integers is " + ( (double) t1 / t2) + " ." ; } 
							}
						else { result = "The modulo of these two integers is " + (t1 % t2) + " ." ;}
					}
				}
				// Update label r after button event has been triggered. 
				r.setText(result) ;
				window.validate() ;
				window.repaint() ;
			}
		}) ;
		window.getContentPane().add(button) ;
		
		// Create label containing result
		r = new JLabel("");
		r.setBounds(94, 157, 300, 28);
		panel.add(r);
		
		// Open window
		window.setSize(400,300) ;
		window.setVisible(true) ;
	}
	
//	#################### MAIN #########################
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		gui_simple_math gui1 = new gui_simple_math() ;
	}
}




/*
 * The only glitch in this one is that modulo is preselected in the combo box, even though sum is what is shown.
 */





