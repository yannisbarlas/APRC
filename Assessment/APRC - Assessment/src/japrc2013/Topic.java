// Examination Number : Y1448283

package japrc2013;

public class Topic implements TopicInterface
{
    private String subject;
    private int duration;
    private CalendarEventInterface topicTarget ;
    
    public Topic(String name, int duration)
    {
        this.subject = name			;
        this.duration = duration	;
        this.topicTarget = null 	;	// Topic's target event
    }

    @Override
    public String getSubject()
    {
        return subject;
    }

    @Override
    public int getDuration()
    {
        return duration;
    }

    // Sets target event of topic
    @Override
    public void setTargetEvent(CalendarEventInterface target)
    {
    	// If event is a valid target, set as topic's target
    	if (target.isValidTopicTarget()) {
    		topicTarget = target ;
    	}
    	// If it's not a valid target, throw exception
    	else { throw new StudyPlannerException("Target type must be exam or essay hand-in.") ; }
    }

    // Gets topic's target event
    @Override
    public CalendarEventInterface getTargetEvent()
    {
        return topicTarget;
    }
}
