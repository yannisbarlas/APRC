// Examination Number : Y1448283

package japrc2013;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class StudyPlanner implements StudyPlannerInterface
{
    private ArrayList<TopicInterface> topics = new ArrayList<TopicInterface>();
    private ArrayList<StudyBlockInterface> plan = new ArrayList<StudyBlockInterface>();
    
    private StudyPlannerGUIInterface gui;
    
    //////////////////////////////////////////////////////////////////////
    // My variables
    private int blockSize   = 60 ;
    private int breakLength = 0 ;
    
    private Calendar start = null ;
    private Calendar end   = null ;
    
    private ArrayList<CalendarEventInterface> calendarEvents = new ArrayList<CalendarEventInterface>() 	;
    private ArrayList<StudyBlockInterface>   otherYearBlocks = new ArrayList<StudyBlockInterface>()		;
    //////////////////////////////////////////////////////////////////////


    public void setGUI(StudyPlannerGUIInterface gui) { this.gui = gui; }
    
    
    
    ///////////////////	TOPIC HANDLING METHODS	////////////////////////
    
    // Adds a topic if a topic with the same name doesn't already exist
    @Override
    public void addTopic(String name, int duration)
    {
    	// Look up all topics and if there is a topic with the same name, throw exception
    	for (TopicInterface t : topics) {
    		if (t.getSubject().equals(name)) {
    			throw new StudyPlannerException ("A topic with that name already exists!") ; 
    		}
    	}
    	
    	// If the topic's study duration is 0 or negative, throw exception
    	if (duration <= 0) { throw new StudyPlannerException("Duration cannot be 0 or negative!") ;}
    	
    	// If the tests above passed, add topic to our list of topics
    	topics.add(new Topic(name,duration)) ;
    }
    
    // Deletes a topic from the topics list
    @Override
    public void deleteTopic(String topic)
    // Takes a string containing the selected topic's name
    {
    	// Use iterator to be able to delete while iterating
    	Iterator<TopicInterface> iter = topics.iterator() ;
    	
    	// Check each topic, find the one whose name is the same as the selected one's, then delete it
    	while (iter.hasNext()) {
    		TopicInterface t = iter.next() ;
    		if ((t.getSubject()).equals(topic)) { 
    			iter.remove() ; 
    		}
    	}
    }

    // Returns the list of all topics
    @Override
    public List<TopicInterface> getTopics() { return topics; }

    
    ///////////////////	 PLAN HANDLING METHODS	////////////////////////
    
    // Returns the list of study blocks that make up the study plan
    @Override
    public List<StudyBlockInterface> getStudyPlan() { return plan; }

    // The method that builds the study plan
    // It is used in both generateStudyPlan methods
    private void generatePlan(Calendar startStudy) {
    	plan = new ArrayList<StudyBlockInterface>() 				;
    	int minHours, minMinutes, maxHours, maxMinutes				;
    	int h, m, day, month										;
    	int lastTopic = -1 											;
    	boolean keep_going = true 									;
		Calendar start = Calendar.getInstance() 					;
		Calendar end   = Calendar.getInstance() 					;
		CalendarEventInterface event 								;
		ArrayList<Integer> minutesLeft = new ArrayList<Integer>() 	;
		ArrayList<Calendar> timeslots = new ArrayList<Calendar>() 	;
		
    	
    	if (topics.size() > 0) {
    		
    		// Get the daily start and end time in integer form
    		minHours 	= getDailyStartStudyTime().get(Calendar.HOUR_OF_DAY) 	;
   			minMinutes 	= getDailyStartStudyTime().get(Calendar.MINUTE) 		;
    		maxHours 	= getDailyEndStudyTime().get(Calendar.HOUR_OF_DAY) 		;
    		maxMinutes 	= getDailyEndStudyTime().get(Calendar.MINUTE) 			;
    		
    		// Get the time the planner is asked to start at in integer form
    		h 	  = startStudy.get(Calendar.HOUR_OF_DAY) 	;
    		m 	  = startStudy.get(Calendar.MINUTE) 		;
    		day   = startStudy.get(Calendar.DAY_OF_MONTH) 	;
    		month = startStudy.get(Calendar.MONTH) 			;
    		
    		// Fix when the plan actually starts
    		// If it starts too early, make it start at daily start study time
    		if (h<minHours) 					{ h = minHours ; m = minMinutes ;}
    		if (h==minHours && m<minMinutes) 	{ m = minMinutes ; }
    		// If it starts too late, start it at daily start study time the next day
    		if (h>maxHours || (h==maxHours && m>maxMinutes)) {
    			addLateEvents(startStudy)	;	// If any events exist between input start time and the next day's strat time, add them
    			day += 1 					; 
    			h = minHours 				; 
    			m = minMinutes				;
    		}
    		
    		// Build a list of integers containing the topic's durations
    		// The values will be gradually reduced, letting the planner know when all minutes of a topic have been studied
    		for ( int i=0 ; i<topics.size() ; i++ ) { minutesLeft.add(topics.get(i).getDuration()) ; }
    		
    		// While loop ends when planner is ready
    		while (keep_going) {
    			// Set two calendars containing the information of when the planner starts and ends on a daily basis
    			setTime(start, month, day, h, m) 				;
    			setTime(end, month, day, maxHours, maxMinutes) 	;
    			
    			// Get the events of the day, if any
    			// How timeslots work is explained in the getTodaysEvents method
    			if (calendarEvents.size()>0) 	{ timeslots = getTodaysEvents(start) ; }
    			else 							{ timeslots = null ; }

    			// If there are NO events today
    			if (timeslots==null) {
	    			planBlock(start, end, minutesLeft, lastTopic)	;	// Plan whole day
	    			lastTopic = getLastTopic() 						;	// Keep track of the last topic used in the planner
	    			day += 1 										;	// Go to the next day
    			}
    			// If there are events today
    			else {
    				// Go through the timeslots
    				for (Calendar cal : timeslots) {
    					// If the timeslot corresponds to an event, add the event to the plan
    					event = isEvent(cal) ;
    					if (event != null) {
    						plan.add ( new StudyBlock (event.getName(), cal, event.getDuration()) ) ;
    					}
    					// If the timeslot corresponds to normal study time
    					else {
    						// Get its position in the timeslot list
    						int pos = timeslots.indexOf(cal) ;
    						
    						// Making sure it is not the last timeslot
    						// If the planner would plan until later than daily end study time, force it to plan until exactly the daily end study time
    						if (pos != timeslots.size()-1 && isDayOver(timeslots.get(pos+1), maxHours, maxMinutes)) {
    							Calendar c = Calendar.getInstance();
    							setTime(c, maxHours, maxMinutes) ;
    							planBlock(timeslots.get(pos), c, minutesLeft, lastTopic);
    							lastTopic = getLastTopic() ;
    						}
    						// Otherwise plan study from this timeslot until the next one
    						else if ( pos != timeslots.size()-1 ) { 
    							planBlock(timeslots.get(pos), timeslots.get(pos+1), minutesLeft, lastTopic);
    							lastTopic = getLastTopic() ;
    						}
    						// If it is the last timeslot, end day
    						else { day +=1 ; }
    					}
    				} // end for loop
    			}
    			
    			// Keep looping only if there are minutes left unstudied in at least one topic
    			keep_going = false ;
    			for (int j=0 ; j<minutesLeft.size() ; j++) {
    				if (minutesLeft.get(j) > 0) {
    					keep_going = true ;
    			}}
    		} // end while loop
    		
    		// If there are events planned for next year, add them
    		if (otherYearBlocks.size() > 0) {
    			for (StudyBlockInterface y : otherYearBlocks) {
    				plan.add(y) ;
    		}}
    		
    		// Notify gui
    		if (gui != null) { gui.notifyModelHasChanged() ; }
    	}
    	// If the list of topics is empty, throw an exception
    	else { throw new StudyPlannerException("No topics have been selected!") ; }
    }
    
    // Creates a study plan starting from the current time
    // Generates a study plan starting from the current time
    @Override
    public void generateStudyPlan()
    {
    	Calendar now = Calendar.getInstance() 	;	// Get current time
    	generatePlan(now) 						;	// Plan from current time
    }

    // Creates a study plan starting from time given as an argument
    // Generates a study plan from the time given in a calendar parameter
    @Override
    public void generateStudyPlan(Calendar startStudy)
    {
    	generatePlan(startStudy) ;
    	
    }
    
    // Method that plans study time between two times (a block of time), given by the Calendar object arguments start and end
    // Arguments minutesLeft and lastTopic are needed as this method will be called repeatedly when generateStudyPlan is used
    private void planBlock (Calendar start, Calendar end, ArrayList<Integer> minutesLeft, int lastTopic) {
    	boolean keep_going = true 	;	// Boolean that checks if the loop should break or not
		boolean makeBreak = false 	;	// Boolean that tells the method whether it should add a break
		int t = 0 					;	// Integer t keeps track of the total minutes since start time, so that the Calendar can be set to the correct time for each study block
		int available = 0 			;	// Integer available calculates how many minutes are left from the current Calendar till end time
		Calendar cal 				;
		
		// Boolean returning true if the last topic used in the planner was NOT the last one in the topics list
		// If it is true, the for loop should start from a different position, not the beginning of the topics list
		// If lastTopic's value is -1, that means we are in the first time this method is used and subsequently there is no last topic used
		boolean hasLast = true ;
		if (lastTopic == -1 || lastTopic == topics.size()-1) { hasLast = false ; }
		
		// Get start time info in integer form
		int month 		= start.get(Calendar.MONTH) 		;
		int day 		= start.get(Calendar.DAY_OF_MONTH) 	;
		int hour 		= start.get(Calendar.HOUR_OF_DAY) 	;
		int minute 		= start.get(Calendar.MINUTE) 		;
		// Get end time info in integer form
		int maxHours 	= end.get(Calendar.HOUR_OF_DAY) 	;
		int maxMinutes 	= end.get(Calendar.MINUTE) 			;

		// The following block of code ensures that if an event precedes the daily start time, a break will be added only if it doesn't end before daily start time
		// That is because breaks are only added in times that are eligible study times
		if (plan.size()>0) {
			// If the last entry in the plan was in the same day
			if (plan.get(plan.size()-1).getStartTime().get(Calendar.DAY_OF_MONTH) == day) {
				
				// Make a calendar object set at the previous entry's end time
				Calendar s = Calendar.getInstance() ;
				setTime(s, plan.get(plan.size()-1).getStartTime().get(Calendar.MONTH), plan.get(plan.size()-1).getStartTime().get(Calendar.DAY_OF_MONTH), plan.get(plan.size()-1).getStartTime().get(Calendar.HOUR_OF_DAY), plan.get(plan.size()-1).getStartTime().get(Calendar.MINUTE)) ;
				s.add(Calendar.MINUTE, plan.get(plan.size()-1).getDuration()) ;
				
				// If the previous entry's end time is the current start time or later than the current start time, add a break first
				if (s.compareTo(start) != -1) {
					Calendar s2 = Calendar.getInstance() ;
					setTime(s2, start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH), start.get(Calendar.HOUR_OF_DAY), start.get(Calendar.MINUTE));
					
					plan.add( new StudyBlock ("break", s2, breakLength) ) ;
					t += breakLength ;
		}}}

		// Loop breaks only when planning for this time block is done
		while (keep_going) {
			// For loop iterates through all topics
			for (int i=0 ; i<topics.size(); i++) {
				// If the study planning shouldn't begin from the first item in the topics list, correct its position
				if (hasLast) {
					i = lastTopic + 1 	;
					hasLast = false 	;
				}
				
				// Set cal to the time the next study block will need to start
				cal = Calendar.getInstance() 								;
				setTime(cal, month, day, hour, minute) 						;
				cal.set(Calendar.MINUTE, (cal.get(Calendar.MINUTE) + t)) 	;
				
				// If the day is over, break the loop
				if (isDayOver(cal, maxHours, maxMinutes)) { keep_going = false ; break ; }
				// If there are no minutes left to study, break the loop
				for (int j=0 ; j<minutesLeft.size() ; j++) { 
					if (minutesLeft.get(j) > 0) { keep_going = true  ; break ; } 
					else 						{ keep_going = false ; }
				}
				
				// Calculate how many minutes are left in the period from start time of this method till the end time
				available = ((maxHours * 60) + maxMinutes) - ((cal.get(Calendar.HOUR_OF_DAY) * 60) + cal.get(Calendar.MINUTE) ) ;
				
				// If there's less than 10 minutes available, do nothing and break the loop
				if (available < 10) {
					keep_going = false ; break ;
				}
				
				// If there's more than 10 minutes, but not enough for a full block size, plan as many minutes as can fit
				else if ((available <= blockSize) && (minutesLeft.get(i)>0)) {
					// Find the smallest value between available minutes and minutes left in the topic
					int dur = 0 ;
					if (available <= minutesLeft.get(i)) { dur = available ; }
					else { dur = minutesLeft.get(i) ; }
					
					// And add that to the plan
					plan.add ( new StudyBlock (topics.get(i).getSubject(), cal, dur) ) 	;	// Add study block to plan
					t += available 														;	// Update value of t
					minutesLeft.set(i, minutesLeft.get(i) - available)					;	// Update value of minutesLeft
				}
				
				// If there is enough time for a full study block
				else if (minutesLeft.get(i) > blockSize) {
					plan.add( new StudyBlock (topics.get(i).getSubject(), cal, blockSize) ) ; 
					t += blockSize 															;
					minutesLeft.set(i, minutesLeft.get(i) - blockSize)						;
					makeBreak = true 														;	// Make sure a break is added
				}
				
				// If there is not enough minutes of study left for a study block
				else if (minutesLeft.get(i)>0 && minutesLeft.get(i)<=blockSize) {
					// If less than 10 minutes of study are left, force the student to study for 10 minutes
					if (minutesLeft.get(i) < 10) { minutesLeft.set(i, 10) ; }
					
					plan.add ( new StudyBlock (topics.get(i).getSubject(), cal, minutesLeft.get(i)) )	;
					t += minutesLeft.get(i) 															;
					minutesLeft.set(i, 0)																;
					
					// If this is not the last entry in the whole plan, make sure a break is added
					for (int j=0 ; j<minutesLeft.size() ; j++) { if (minutesLeft.get(j) > 0) { makeBreak = true ; } }
				}
				
				// Add a break if it is not set to 0
				if (breakLength > 0 && makeBreak) {
					// Set a calendar object to the break's start time
					day   = cal.get(Calendar.DAY_OF_MONTH) 	;
					month = cal.get(Calendar.MONTH) 		;
					
					cal = Calendar.getInstance() 								;
					setTime(cal, month, day, hour, minute) 						;
					cal.set(Calendar.MINUTE, (cal.get(Calendar.MINUTE) + t)) 	;
					
					// If there is enough time for a full break, add it
					if (available - plan.get(plan.size()-1).getDuration() >= breakLength) {
						plan.add( new StudyBlock ("break", cal, breakLength) ) ;
					}
					// Otherwise plan a shorter one
					else {
						plan.add( new StudyBlock ("break", cal, available - plan.get(plan.size()-1).getDuration()) ) ;
					}
					
					t += breakLength 	;
					makeBreak = false 	;	// Make sure that breaks don't keep getting added, unless explicitly instructed to do so
				}
			} // end for loop
		} // end while loop
    }
    
    // Gets the last used topic in the plan, in order to continue planning from the one after it
    
    private int getLastTopic() {
    	if (plan.size()>0) {
    		// Get the name of the last entry of the day
	    	String last = plan.get(plan.size()-1).getTopic() ;
	    	// Unless that entry is a break, in which case, get the one before the last one
	    	if (last == "break") { last = plan.get(plan.size()-2).getTopic() ; }
	    	
	    	// Find the position of that topic in the topics list and return it 
	    	for (int i=0 ; i<topics.size() ; i++) {
	    		if (topics.get(i).getSubject().equals(last)) {return i ;}
	    	}
	    	return 0 ;
    	}
    	return -1 ;
    }
    
    // Checks a time and returns whether it is after the daily end study time    

    private boolean isDayOver(Calendar cal, int maxHours, int maxMinutes) {
    	if (cal.get(Calendar.HOUR_OF_DAY) > maxHours) 											{ return true ; }
    	if (cal.get(Calendar.HOUR_OF_DAY) == maxHours && cal.get(Calendar.MINUTE) > maxMinutes) { return true ; } 
    	
    	return false ;
    }


    ///////////////////	 EVENT HANDLING METHODS	////////////////////////
    
    // Adds a calendar event of type OTHER
    @Override
    public void addCalendarEvent(String eventName, Calendar startTime, int duration)
    {
    	// If the event's time overlaps with another event, throw exception
    	if (doesOverlap(startTime, duration)) {
    		throw new StudyPlannerException("Cannot add an event for this time.") ;
    	}
    	else {
    		// Make sure seconds and milliseconds are set to 0
    		startTime.set(Calendar.SECOND, 0) ;
    		startTime.set(Calendar.MILLISECOND, 0) ;
    		
    		// Add event and notify gui
    		calendarEvents.add( new CalendarEvent(eventName, startTime, duration, CalendarEventType.OTHER) ) ;
    		if (gui != null) { gui.notifyModelHasChanged() ; }
    	}
    }

    // Adds a calendar event of type EXAM or ESSAY
    @Override
    public void addCalendarEvent(String eventName, Calendar startTime, int duration, CalendarEventType type)
    {
    	// If the event's time overlaps with another event, throw exception
    	if (doesOverlap(startTime, duration)) {
    		throw new StudyPlannerException("Cannot add an event for this time.") ;
    	}
    	else {
    		// Make sure seconds and milliseconds are set to 0
    		startTime.set(Calendar.SECOND, 0) ;
    		startTime.set(Calendar.MILLISECOND, 0) ;
    		
    		// Add event and notify gui
    		calendarEvents.add( new CalendarEvent(eventName, startTime, duration, type) ) ;
    		if (gui != null) { gui.notifyModelHasChanged() ; }
    	}
    }
    
    // Takes the start time and duration of a calendar event as arguments
    // Returns true if it overlaps with other events
    public boolean doesOverlap(Calendar start, int duration) {
    	Calendar eventStart ;
    	Calendar eventEnd 	;
    	
    	// Get event's start time info in integer form
    	int month = start.get(Calendar.MONTH) 		;
    	int day = start.get(Calendar.DAY_OF_MONTH) 	;
    	int hour = start.get(Calendar.HOUR_OF_DAY) 	;
    	int minute = start.get(Calendar.MINUTE) 	;
    	
    	// Make a calendar object set to the event's end time 
    	Calendar end = Calendar.getInstance() 	;
    	setTime(end, month, day, hour, minute) 	;
    	end.add(Calendar.MINUTE, duration)		;

    	// Cycle through all events and check if it overlaps with any of them
    	for (CalendarEventInterface c : calendarEvents) {
    		// Make a calendar with the current event's (in the for loop) start time
    		eventStart = c.getStartTime() ;
    		
    		// Make a calendar with the current event's end time
    		eventEnd = Calendar.getInstance() 					;
    		int m = c.getStartTime().get(Calendar.MONTH) 		;
    		int d = c.getStartTime().get(Calendar.DAY_OF_MONTH) ;
    		int h = c.getStartTime().get(Calendar.HOUR_OF_DAY) 	;
    		int mn = c.getStartTime().get(Calendar.MINUTE) 		;
    		setTime(eventEnd, m, d, h, mn) 						;
    		eventEnd.add(Calendar.MINUTE, c.getDuration()-1)	;	// subtract one minute so that we can have two consecutive events (eg. at 9 for 60 min and then at 10)

    		// If the selected event overlaps with the event from the argument, return true
    		if ((start.compareTo(eventStart)==1 && start.compareTo(eventEnd)==-1) || ((end.compareTo(eventStart)==1 && end.compareTo(eventEnd)==-1))) { return true ; }
    	}
    	
    	return false ;
    }

    @Override
    public List<CalendarEventInterface> getCalendarEvents() { return calendarEvents ; }
    
    /*
     * Gets the events that exist in a particular day
     * If there are any, it breaks the day into a list of "timeslots"
     * Timeslots are calendar objects. The format is [9.00, 10.00, 15.00 etc.]
     * The planning method loops through them and sees if they are an event or not
     * If they are, it adds an event to the plan and moves on to the next one
     * If they are not, it plans study time between the current one and the next one
     * The list always ends with an 'unused' timeslot, marking the end of the day
     * This behaviour can be seen in the generatePlan method. The following method is where they get calculated
     */

    private ArrayList<Calendar> getTodaysEvents(Calendar start) {
    	ArrayList<Calendar> todaysEvents = new ArrayList<Calendar>()	;	// The list that will be returned
    	Calendar temp 													;	// Helper calendar object
    	Calendar last = Calendar.getInstance() 							;	// Calendar object keeping track of the last event's end time
    	Calendar end = getDailyEndStudyTime() 							;	// Calendar object indicating when the daily end study time is
    	boolean startChanged = false 									;	// Boolean that checks if the beginning of the day should be moved forward because of an event
    	
    	// Make sure end is at the same date as start
    	end.set (Calendar.MONTH, start.get(Calendar.MONTH))				  ;
    	end.set (Calendar.DAY_OF_MONTH, start.get(Calendar.DAY_OF_MONTH)) ;
    	
    	// Cycle through all calendar events and find the ones that are today
    	for (CalendarEventInterface c : calendarEvents) {
    		// If event c is today
    		if (isToday(c, start)) {
    			// Add event's start time
    			todaysEvents.add(c.getStartTime()) ;
    			
    			// Remove duplicates
    			if (todaysEvents.size()>2) {
    				if (todaysEvents.get(todaysEvents.size()-1).compareTo(todaysEvents.get(todaysEvents.size()-2)) == 0) {
    					todaysEvents.remove(c.getStartTime()) ;
    			}}
    			
    			// Make a calendar with event's end time
    			temp = Calendar.getInstance() ;
    			setTime(temp, start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH), c.getStartTime().get(Calendar.HOUR_OF_DAY), c.getStartTime().get(Calendar.MINUTE)) ;
    			temp.add(Calendar.MINUTE, c.getDuration()) ;
    			
    			// If the event starts before or on daily end time, but ends after daily end time, add its end to the list 
    			if (!(c.getStartTime().compareTo(end) == 1 && temp.compareTo(end) == 1)) {
        			if (temp.compareTo(start) == 1) { todaysEvents.add(temp) ; }
    			}

    			// If the event started before the daily start study time and ended after the daily start study time, note that the day begins later than normal
    			if (c.getStartTime().compareTo(start) == -1 && temp.compareTo(start) == 1) { startChanged = true ; }
    			
    			last = temp ;
    		}
    	}
    	
    	if (todaysEvents.size() > 0) {
    		// If the daily start study time hasn't been added to the timeslots yet, add it
    		if (!startChanged) { todaysEvents.add(start) ; }
    		// Sort all events of the day in chronological order
    		sortEvents(todaysEvents) ;
    		// If the last timeslot is before the day ends, add daily end study time
    		if (todaysEvents.get(todaysEvents.size()-1).compareTo(end) == -1) {
    			temp = Calendar.getInstance() ;
    			setTime(temp, start.get(Calendar.MONTH), start.get(Calendar.DAY_OF_MONTH), getDailyEndStudyTime().get(Calendar.HOUR_OF_DAY), getDailyEndStudyTime().get(Calendar.MINUTE)) ;
    			todaysEvents.add(temp) ;
    		}
    		// If the last timeslot is after the day ends 
    		else {
    			// And if last hasn't been added to the timeslots already, add it
    			if (todaysEvents.get(todaysEvents.size()-1).compareTo(last) != 0) {
    				todaysEvents.add(last) ;
    	}}}
    	// If there are no events today, set the list to null
    	else { todaysEvents = null ; }
    	
    	return todaysEvents ;
    }
    
    // Takes a list of times (of events) and sorts them in chronological order
    private void sortEvents(ArrayList<Calendar> eventTimes) {
    	boolean keep_going = true 	;
    	Calendar temp 				;
    	
    	while (keep_going) {
    		keep_going = false ;
    		// Cycle through the list of times
    		for (int i=0 ; i<eventTimes.size()-1 ; i++) {
    			// If the time is greater than the one after it, swap their positions
    			if (eventTimes.get(i).compareTo(eventTimes.get(i+1))==1) {
    				temp = eventTimes.get(i) 				;
    				eventTimes.set(i, eventTimes.get(i+1)) 	;
    				eventTimes.set(i+1, temp) 				;
    				keep_going = true 						;
    	}}}
    }
    
    // Gets a calendar time and sees if there is an event scheduled for that time
    private CalendarEventInterface isEvent (Calendar c) {
    	// Cycle through all events
    	for (CalendarEventInterface e : calendarEvents) {
    		// If the event's month is c's month 
    		if (e.getStartTime().get(Calendar.MONTH)==c.get(Calendar.MONTH)) {
    			// If the event's day is c's day
    			if (e.getStartTime().get(Calendar.DAY_OF_MONTH)==c.get(Calendar.DAY_OF_MONTH)) {
    				// If the event's hour is c's hour
    				if (e.getStartTime().get(Calendar.HOUR_OF_DAY)==c.get(Calendar.HOUR_OF_DAY)) {
    					// And if the event's minute is c's minute
    					if (e.getStartTime().get(Calendar.MINUTE)==c.get(Calendar.MINUTE)) {
    						// Then give me that event
    						return e ;
    	}}}}}
    	
    	// Otherwise return null
    	return null ;
    }
    
    // Takes a calendar noting the date today and an event
    // Determines whether that event is scheduled for today
    private boolean isToday(CalendarEventInterface c, Calendar cal) {
    	// If the event is today, return true
    	if (	         c.getStartTime().get(Calendar.YEAR) == cal.get(Calendar.YEAR)
    		 &&         c.getStartTime().get(Calendar.MONTH) == cal.get(Calendar.MONTH) 
    		 &&  c.getStartTime().get(Calendar.DAY_OF_MONTH) == cal.get(Calendar.DAY_OF_MONTH))
    	{
    		return true ;
    	}
    	
    	// If the event is of the same day, but in a DIFFERENT YEAR, add the event to the list dedicated for these cases
    	else if (		         c.getStartTime().get(Calendar.YEAR) >  cal.get(Calendar.YEAR)
       		 	 	 &&         c.getStartTime().get(Calendar.MONTH) == cal.get(Calendar.MONTH) 
       		 	 	 &&  c.getStartTime().get(Calendar.DAY_OF_MONTH) == cal.get(Calendar.DAY_OF_MONTH) ) 
    	{
    		otherYearBlocks.add ( new StudyBlock (c.getName(), c.getStartTime(), c.getDuration()) ) ;
    	}
    	
    	// Otherwise return false
    	return false ;
    }
    
    // Deals with the case where our start time for planning is after daily end study time, 
    // but there are still events scheduled after start time and before the next daily start study time 
    private void addLateEvents(Calendar cal) {
    	// Cycle through all calendar events
    	for (CalendarEventInterface c : calendarEvents) {
    		// If event is today 
    		if (isToday(c, cal)) {
    			// If event is not before the planner's start time, add it to the plan
    			if (c.getStartTime().compareTo(cal) != -1) {
    				plan.add ( new StudyBlock (c.getName(), c.getStartTime(), c.getDuration()) ) ;
    	}}}
    }
    

    ///////////////////	 CALENDAR TIME METHODS	////////////////////////

    /*
     * Two methods doing the same thing, but with different arguments for the convenience of not having to get the month and day if they are not needed
     * They get a calendar object and set its time (and date) to the time given in the arguments
     * They also make sure that all created calendar objects have a second and millisecond value of 0
     */
    
    private void setTime(Calendar c, int hour, int minute) {
		c.set(Calendar.HOUR_OF_DAY, hour) ;
    	c.set(Calendar.MINUTE, minute) ;
    	c.set(Calendar.SECOND,0) ;
    	c.set(Calendar.MILLISECOND, 0) ;
    }
    
    private void setTime(Calendar c, int month, int day, int hour, int minute) {
    	c.set(Calendar.MONTH, month) ;
    	c.set(Calendar.DAY_OF_MONTH, day) ;
    	c.set(Calendar.HOUR_OF_DAY, hour) ;
    	c.set(Calendar.MINUTE, minute) ;
    	c.set(Calendar.SECOND,0) ;
    	c.set(Calendar.MILLISECOND, 0) ;
    }
    

    //////////////	BLOCK SIZE & BREAK LENGTH METHODS	///////////////////
    
    
    // Sets study block size
    @Override
    public void setBlockSize(int size) {
    	// Only if the block size is at least 10 minutes
    	if (size<10) { throw new StudyPlannerException("Plase choose a bigger size for the study block.") ; }
    	blockSize = size ; 
    }

    // Sets break length
    // Set break length
    @Override
    public void setBreakLength(int length) {
    	// Making sure that break length is not negative
    	if (length < 0) { throw new StudyPlannerException("Break length cannot have a negative value!") ; }
    	breakLength = length ;
    }

    
    ////////////////  	DAILY START & END METHODS	///////////////////
    
    // Sets daily start study time
    @Override
    public void setDailyStartStudyTime(Calendar startTime)
    {
    	// Making sure that daily end study time is not before daily start study time and that a full study block can fit between them 
    	Calendar e = getDailyEndStudyTime() ;
    	int diff = (e.get(Calendar.HOUR_OF_DAY)*60 + e.get(Calendar.MINUTE)) - (startTime.get(Calendar.HOUR_OF_DAY) * 60 + startTime.get(Calendar.MINUTE)) ;
    	if (diff < blockSize) { throw new StudyPlannerException("You need to set a better start time.") ; }
    	
		// Make sure that seconds and milliseconds have a value of 0 
		startTime.set(Calendar.SECOND, 0) 		;
		startTime.set(Calendar.MILLISECOND, 0) 	;
		
		start = startTime ;
    }

    // Sets daily end study time
    @Override
    public void setDailyEndStudyTime(Calendar endTime)
    {
    	// Making sure that daily end study time is not before daily start study time and that a full study block can fit between them
    	Calendar s = getDailyStartStudyTime() ;
    	int diff = (endTime.get(Calendar.HOUR_OF_DAY)*60) + endTime.get(Calendar.MINUTE) - ( (s.get(Calendar.HOUR_OF_DAY) * 60) + s.get(Calendar.MINUTE) ) ;
    	if (diff < blockSize) { throw new StudyPlannerException("You need to set a better end time.") ; }
    	
    	// Make sure that seconds and milliseconds have a value of 0
    	endTime.set(Calendar.SECOND, 0) 		;
    	endTime.set(Calendar.MILLISECOND, 0) 	;
    	
    	end = endTime ; 
    }

    // Gets the daily start study time
    @Override
    public Calendar getDailyStartStudyTime()
    {
    	// If it hasn't been set to a different time, return the default one
        if (start==null) {
        	Calendar c = Calendar.getInstance() ;
        	setTime(c,9,0)						;
        	return c 							;
        }
        else { return start; }
    }

    // Gets the daily end study time
    @Override
    public Calendar getDailyEndStudyTime()
    {
    	// If it hasn't been set to a different time, return the default one
    	if (end == null) {
    		Calendar c = Calendar.getInstance() ;
    		setTime(c, 17, 0) 					;
    		return c 							;
    	}
    	else { return end ;}
    }

    
    //////////////////	FILE HANDLING METHODS	///////////////////////

    // Saves the current study plan to a file
    // The format used is [ month, day, hour, minute, topic, duration, separated by ";" ] for each line of the planner 
    @Override
    public void saveData(OutputStream saveStream)
    {
    	SimpleDateFormat df = new SimpleDateFormat("MM;dd;HH;mm") ;
    	PrintWriter pw = new PrintWriter(saveStream) ;
    	
    	// For each study block in the plan, add a line to the file
    	for (StudyBlockInterface s : plan) {
    		pw.write( df.format(s.getStartTime().getTime()) + ";" + s.getTopic() + ";" + s.getDuration() + ";" +"\n") ;
    	}
    	
		pw.close();
    }

    // Gets data from a file and fills the plan list with that data 
    @Override
    public void loadData(InputStream loadStream)
    {
    	int mn, d, h, m, duration 				;
    	String subject 							;
    	Calendar cal = Calendar.getInstance() 	; 
    	
    	try {
    		// If the file is empty, throw exception
			if (loadStream.available()==0) { throw new StudyPlannerException("Please load a non-empty file!") ; }
			
			try {
				BufferedReader reader = new BufferedReader( new InputStreamReader (loadStream) );
		    	
				plan = new ArrayList<StudyBlockInterface> () ;
		    	
				// Look at each line in the file
		    	Scanner scanner = new Scanner(reader) ;
		    	while (scanner.hasNextLine()) {
		    		// Separate the information
		    		String[] s = scanner.nextLine().split(";") ;
		    		
		    		// If there is information missing from a line, throw exception
		    		if (s.length != 6) { throw new StudyPlannerException("Please load a valid file!") ; }

		    		mn = Integer.parseInt(s[0])			;	// Get the month
		    		d = Integer.parseInt(s[1])			;	// Get the day
		    		h = Integer.parseInt(s[2])			;	// Get the hour
		    		m = Integer.parseInt(s[3])			;	// Get the minute
		    		setTime(cal, mn, d, h, m) 			;	// Make a calendar with the above information (to be used as a study block's start time)
		    		
		    		subject = s[4]						;	// Get the subject
		    		duration = Integer.parseInt(s[5])	;	// Get the duration
		    		
		    		// Add a study block to the plan with the subject, calendar and duration from above
		    		plan.add( new StudyBlock(subject, cal, duration) ) ;
		    		
		    	} // end while loop
			}
			// If any of the data that should be an integer is not, throw exception
			catch (NumberFormatException e) { throw new StudyPlannerException("Please make sure the date and duration in the file are integers!") ; }
		} 
    	
    	catch (IOException e) { e.printStackTrace(); }
    }


}
