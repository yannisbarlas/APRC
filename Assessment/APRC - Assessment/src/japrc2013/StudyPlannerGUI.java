// Examination Number : Y1448283

package japrc2013;



import japrc2013.StudyPlannerInterface.CalendarEventType;

import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.FileChooserUI;

public final class StudyPlannerGUI extends JFrame implements StudyPlannerGUIInterface
{
    private JButton generateButton;
    private JButton exitButton;
    private JList<String> topicList;
    private JList<String> studyPlan;
    private JLabel topicLabel;
    private JLabel planLabel;
    
    /////////////////////////////////////////////////////////
    				// My declarations //
    /////////////////////////////////////////////////////////
    private JLabel event, colon, comment, targetLabel ;
    private JTextField newName, newHours, blockSize, breakLength, eventName, startDate, startHour, startMinute, eventDuration ;
    private JButton addNewTopic, deleteTopic, updateDurations, addEvent, giveTarget, savePlan, loadPlan ;
    private JComboBox<String> eventType ;
    private String selectedTopic = "", selectedEvent = "" ;
    private Calendar calendar = Calendar.getInstance() ;
    private JList<String> eventList ;
    private JScrollPane validEvents ;
    private JFileChooser filechooser ;
    /////////////////////////////////////////////////////////
    
   
    private StudyPlanner planner;
    
    public StudyPlannerGUI(StudyPlanner simToUse)
    {
        super("Study Planner");
        setLayout(null) ;
     
        this.planner = simToUse;
        
        generateButton = new JButton("Generate Study Plan");
        generateButton.setBounds(10,10,180,30) ;
        generateButton.addActionListener(new ActionListener() {            
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
            	if (planner.getTopics().size() < 1) {
            		JOptionPane.showMessageDialog(generateButton, "You need to first add some topics!", "Error", JOptionPane.PLAIN_MESSAGE);
            	}
            	planner.generateStudyPlan();
            }
        });
        add(generateButton);
        
        exitButton = new JButton("Exit Program");
        exitButton.setBounds(10, 45, 180, 30);
        exitButton.addActionListener(new ActionListener() {            
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                System.exit(0);               
            }
        });
        add(exitButton);        

        
        topicLabel = new JLabel("Topics:");
        topicLabel.setBounds(220, 10, 100, 20) ;
        add(topicLabel);
        
        String[] data = {"one", "two", "three", "four"};
        topicList = new JList<String>(data);
        JScrollPane sp1 = new JScrollPane(topicList) ;
        sp1.setBounds(270,10,400,100);
        add(sp1) ;
        
        planLabel = new JLabel("Study Plan:");
        planLabel.setBounds(220,140,80,20) ;
        add(planLabel);
        
        data = new String[] {"one", "two", "three", "four"};
        studyPlan = new JList<String>(data);
        JScrollPane sp2 = new JScrollPane(studyPlan) ;
        sp2.setBounds(310, 140, 400, 500) ;
        add(sp2);
        
        
        /////////////////////////////////////////////////////////////
        					// MY GUI ADDITIONS //
        /////////////////////////////////////////////////////////////
        
        // ###### -- Add a new topic -- #####
        
        // Text field containing new topic's name 
        newName = new JTextField ("Enter new topic's name") ;
        newName.addMouseListener(new MouseAdapter(){
            @Override
            // delete text when text field clicked
            public void mouseClicked(MouseEvent e)	{ newName.setText(""); }	});
        newName.setBounds (10,180,146,20) ;
        add(newName) ;
        
        // Text field containing new topic's duration
        newHours = new JTextField ("Enter new topic's duration") ;
        newHours.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ newHours.setText(""); }	});
        newHours.setBounds (10,200,146,20) ;
        add(newHours) ;
        
        // Button that adds a new topic
        addNewTopic = new JButton("Add") ;
        addNewTopic.addActionListener (new ActionListener() {
        	@Override
        	public void actionPerformed (ActionEvent arg0) {
        		try {
        			// If new topic's name is empty, throw error 
        			if (newName.getText().equals("") || newName.getText().equals("Enter new topic's name")) {
        				JOptionPane.showMessageDialog(addNewTopic, "Please give the new topic a name!", "Error", JOptionPane.PLAIN_MESSAGE);
        			}
        			// If new topic's duration is empty, throw error
        			else if (newHours.getText().equals("")) {
        				JOptionPane.showMessageDialog(addNewTopic, "Please enter the duration of the new topic!", "Error", JOptionPane.PLAIN_MESSAGE);
        			}
        			// If new topic's duration is negative or 0, throw error
        			else if (Integer.parseInt(newHours.getText()) <= 0) {
        				JOptionPane.showMessageDialog(addNewTopic, "Topic's duration must be a positive number!", "Error", JOptionPane.PLAIN_MESSAGE);
        			}
        			// If input is good, add new topic 
        			else { 
        				planner.addTopic(newName.getText(), Integer.parseInt(newHours.getText()));
        				notifyModelHasChanged() ;
        			}
        		}
        		catch (NumberFormatException e) {
        			// If new topic's duration is not an integer, throw error
        			JOptionPane.showMessageDialog(addNewTopic, "The duration of the new topic must be an integer!", "Error", JOptionPane.PLAIN_MESSAGE);
        		}
        	}
        }) ;
        addNewTopic.setBounds(28, 230, 100, 20) ;
        add(addNewTopic) ;
        
        
        // ##### -- Delete a topic from the list -- #####
        
        // List selection listener to see what the selected topic is
        ListSelectionListener listen = new ListSelectionListener() {
        	@Override
			public void valueChanged(ListSelectionEvent e) {
        		if (topicList.getSelectedValue() != null) 	{ selectedTopic = topicList.getSelectedValue().split(" \\(")[0] ; }		}};
        topicList.addListSelectionListener(listen) ;
        
        // Button that deletes selected topic
        deleteTopic = new JButton ("Delete selected topic") ;
        deleteTopic.addActionListener( new ActionListener() {
        	@Override
        	public void actionPerformed (ActionEvent arg0) {
        		planner.deleteTopic(selectedTopic);
        		notifyModelHasChanged() ;
        	}});
        deleteTopic.setBounds(10, 300, 170, 20);
        add(deleteTopic) ;
        
        
        // ##### -- Change block size and break length -- #####
        
        // Block size text field
        blockSize = new JTextField("Enter new study block length") ;
        blockSize.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ blockSize.setText(""); }	});
        blockSize.setBounds(10,400,180,20) ;
        add(blockSize) ;
        
        // Break length text field
        breakLength = new JTextField("Enter new break length") ;
        breakLength.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e){ breakLength.setText(""); }	});
        breakLength.setBounds(10,420,180,20);
        add(breakLength) ;
        
        // Update durations button
        updateDurations = new JButton("Update durations") ;
        updateDurations.addActionListener( new ActionListener() {
        	@Override
        	public void actionPerformed (ActionEvent arg0) {
        		try {
        			// If input is good, update durations
        			if (Integer.parseInt(blockSize.getText()) >= 10 && Integer.parseInt(breakLength.getText()) >= 0) {
        				planner.setBlockSize(Integer.parseInt(blockSize.getText())) ;
            			planner.setBreakLength(Integer.parseInt(breakLength.getText())) ;
            			JOptionPane.showMessageDialog(updateDurations, "Durations successfully updated!", "Success!", JOptionPane.PLAIN_MESSAGE);
        			}
        			// If block size is not above minimum length, throw error
        			else if (Integer.parseInt(blockSize.getText()) < 10) {
            			JOptionPane.showMessageDialog(updateDurations, "Block size must be at least 10 minutes!", "Error", JOptionPane.PLAIN_MESSAGE);
        			}
        			// If break length is negative, throw error
        			else if (Integer.parseInt(breakLength.getText()) < 0) {
        				JOptionPane.showMessageDialog(updateDurations, "Break length cannot be negative!", "Error", JOptionPane.PLAIN_MESSAGE);
        			}
        		}
        		catch (NumberFormatException e) {
        			// If one of the inputs is not an integer, throw error
        			JOptionPane.showMessageDialog(updateDurations, "Input must be an integer!", "Error", JOptionPane.PLAIN_MESSAGE);
        		}
        	}
        });
        updateDurations.setBounds(10,450,140,20);
        add(updateDurations) ;
        
        
        // ##### -- Add a new calendar event -- #####
        
        // Label / Header
        event = new JLabel("Add a new calendar event") ;
        event.setBounds(850, 20, 180, 20);
        add(event) ;
        
        // Event name text field
        eventName = new JTextField("Enter event name") ;
        eventName.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ eventName.setText(""); }	});
        eventName.setBounds(850, 40, 180, 20) ;
        add(eventName) ;
        
        // Event date text field
        startDate = new JTextField("Enter event date (mm/dd)") ;
        startDate.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ startDate.setText(""); }	});
        startDate.setBounds(850, 60, 180, 20);
        add(startDate);
        
        // Event hour text field
        startHour = new JTextField("Start hour") ;
        startHour.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ startHour.setText(""); }	});
        startHour.setBounds(850, 80, 80, 20) ;
        add(startHour) ;
        
        // JLabel for ":"
        colon = new JLabel(":");
        colon.setBounds(938, 80, 20, 20) ;
        add(colon) ;
        
        // Event minute text field
        startMinute = new JTextField("Start minute") ;
        startMinute.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ startMinute.setText(""); }	});
        startMinute.setBounds(950, 80, 80, 20) ;
        add(startMinute) ;
        
        // Explanatory comment JLabel
        comment = new JLabel("(Use 24-hour format)") ;
        comment.setBounds(1050, 80, 150, 20) ;
        add(comment) ;
         
        // Event duration text field
        eventDuration = new JTextField("Enter event duration") ;
        eventDuration.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e)	{ eventDuration.setText(""); }	});
        eventDuration.setBounds(850, 100, 180, 20);
        add(eventDuration) ;
        
        // JComboBox with possible event types
        String[] types = new String[] {"Exam", "Essay Hand-in", "Other"} ;
        eventType = new JComboBox<String> (types) ;
        eventType.setBounds(850, 120, 180, 20);
        add(eventType) ;
        
        // Add event button
        addEvent = new JButton("Add event") ;
        addEvent.addActionListener( new ActionListener() {
        	@Override
        	public void actionPerformed (ActionEvent arg0) {
        		// Make event constructor's data (see dedicated methods)
        		makeEventCalendar() ;
        		if (calendar == null) { throw new StudyPlannerException("Invalid date!") ; }	// Make sure event is not created if calendar is invalid
        		
        		int      			dur  = makeDuration() 		;
        		CalendarEventType 	type = makeType() 			;
        		
        		// If event's time is not acceptable, throw error
        		if (planner.doesOverlap(calendar, dur)) {
        			JOptionPane.showMessageDialog(addEvent, "Cannot add an event for this time!", "Error", JOptionPane.PLAIN_MESSAGE);
        		}
        		
        		// Use appropriate constructor to create event
        		if (type == CalendarEventType.OTHER) { planner.addCalendarEvent(eventName.getText(), calendar, dur); }
        		else { planner.addCalendarEvent(eventName.getText(), calendar, dur, type); }
        	}
        });
        addEvent.setBounds(885, 150, 100, 20) ;
        add(addEvent) ;
        
        
        // ##### -- Set target of topic -- #####
        
        // Label / Header
        targetLabel = new JLabel("Set target event for the selected topic") ;
        targetLabel.setBounds(850,250,250,20) ;
        add(targetLabel);
        
        // Make a list to hold all events of types EXAM and ESSAy
        eventList = new JList<String>() ;
        ListSelectionListener listenEvents = new ListSelectionListener() {
        	@Override
        	// Get selected event from the list in a variable
			public void valueChanged(ListSelectionEvent e) {
        		if (eventList.getSelectedValue() != null) 	{ selectedEvent = eventList.getSelectedValue() ; }		}};
        eventList.addListSelectionListener(listenEvents) ;
        
        // Put list into a scollpane
        validEvents = new JScrollPane(eventList) ;
        validEvents.setBounds(850,270,250,200);
        add(validEvents) ;
        
        // Set target event button
        giveTarget = new JButton("Set target event");
        giveTarget.addActionListener( new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent arg0) {
        		TopicInterface t 		  = null ;
        		CalendarEventInterface ce = null ;
        		
        		// Get topic object from selected topic
        		for (TopicInterface topic : planner.getTopics()) {
        			if (topic.getSubject().equals(selectedTopic)) {
        				t = topic ;
        		}}
        		
        		// Get event object from selected event
        		for (CalendarEventInterface c : planner.getCalendarEvents()) {
        			if (c.getName().equals(selectedEvent)) {
        				ce = c ;
        		}}
        		
        		// Set selected event as selected topic's target 
        		t.setTargetEvent(ce);
        	}
        });
        giveTarget.setBounds(850,480,250,20);
        add(giveTarget) ;

        // ##### -- File Handling -- #####
        
        // Make file chooser component
        filechooser = new JFileChooser(System.getProperty("user.dir")) ;
        
        // Save plan button
        savePlan = new JButton("Save plan") ;
        savePlan.addActionListener( new ActionListener() {
        	@Override
        	public void actionPerformed (ActionEvent arg0) {
        		// If plan is empty, throw error
        		if (planner.getStudyPlan().size() == 0) {
					JOptionPane.showMessageDialog(savePlan, "There is no data in the plan!", "Error", JOptionPane.PLAIN_MESSAGE);
				}
        		else {
        			// Open save dialog and save data to file
	        		filechooser.showSaveDialog(getParent()) ;
	        		try {
						FileOutputStream stream = new FileOutputStream (filechooser.getSelectedFile()) ;
						planner.saveData(stream);
					} catch (FileNotFoundException e) { e.printStackTrace(); }
        		}
        	}});
        savePlan.setBounds(850,600,250,20);
        add(savePlan) ;
        
        // Load plan button
        loadPlan = new JButton("Load plan");
        loadPlan.addActionListener( new ActionListener() {
        	@Override
        	public void actionPerformed (ActionEvent arg0) {
        		// Open load dialog
        		filechooser.showOpenDialog(getParent()) ;
        		try {
					InputStream input = new FileInputStream(filechooser.getSelectedFile()) ;
					// If chosen file is empty, throw error
					if (input.available() == 0) {
						JOptionPane.showMessageDialog(loadPlan, "Input file is empty!", "Error", JOptionPane.PLAIN_MESSAGE);
					}
					else {
						// If no problems occured, load file
						planner.loadData(input);
					}
				} catch (HeadlessException | IOException e) { e.printStackTrace(); }
        		catch (StudyPlannerException e) {
        			// If studyPlannerException from StudyPlanner object's loadData method is thrown, throw error 
        			JOptionPane.showMessageDialog(loadPlan, "Input file is invalid!", "Error", JOptionPane.PLAIN_MESSAGE);
        		}
        	}
        });
        loadPlan.setBounds(850,625,250,20);
        add(loadPlan) ;
        
    }
    
    // Gets event input's date and time data and makes a calendar to be passed in as an argument for the event's constructor
    private void makeEventCalendar() {
    	try {
    		// Parse hour and minute text field input as integers
    		int h = Integer.parseInt(startHour.getText()) ;
    		int m = Integer.parseInt(startMinute.getText()) ;

    		// Parse date text field input as integers
    		String[] date = startDate.getText().split("/");
    		int mon = Integer.parseInt(date[0]) ;
    		int day = Integer.parseInt(date[1]) ;
    		
    		// If date and time input is invalid, throw error and set calendar to null
    		if (mon<1 || mon>12 || (mon==2 && day>29) || ((mon==4 || mon==6 || mon==9 || mon==11) && day>30) || day<1 || h<0 || h>23 || m<0 || m>59) { 
    			JOptionPane.showMessageDialog(addEvent, "Enter a valid date and time!", "Error", JOptionPane.PLAIN_MESSAGE);
    			calendar = null ;
    		}
    		// If all input data is good, set calendar
    		else {
    			Calendar cal = Calendar.getInstance() ;
        		cal.set(Calendar.MONTH, mon-1);
        		cal.set(Calendar.DAY_OF_MONTH, day);
        		cal.set(Calendar.HOUR_OF_DAY, h) ;
        		cal.set(Calendar.MINUTE, m);
        		cal.set(Calendar.SECOND, 0);
        		
        		calendar = cal ;
    		}
    	}
    	catch (NumberFormatException e) { 
    		// If input is not an integer, throw error
    		JOptionPane.showMessageDialog(addEvent, "Enter a valid date and time!", "Error", JOptionPane.PLAIN_MESSAGE);
    	}
    }
      
    // Gets event duration text field's input and turns it into an integer
    private int makeDuration() {
    	try { 
    		int dur = Integer.parseInt(eventDuration.getText()) ;
    		return dur ;
    	}
    	// If input is not an integer, throw error
		catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(addEvent, "Duration must be an integer!", "Error", JOptionPane.PLAIN_MESSAGE);
			throw new StudyPlannerException("Enter a valid duration") ;
		}
    }
    
    // Gets event type choice from gui and creates a CalendarEventType for use in an event constructor
    private CalendarEventType makeType() {
    	CalendarEventType type ;
		String t = (String) eventType.getSelectedItem() ;
		
		if 		(t=="Exam") 			{ type = CalendarEventType.EXAM ; }
		else if (t=="Essay Hand-in") 	{ type = CalendarEventType.ESSAY ; }
		else 							{ type = CalendarEventType.OTHER ; }
		
		return type ;
    }
    
    @Override
    public void notifyModelHasChanged()
    {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                updateDisplay();
            }
        });
    }
    
    private void updateDisplay()
    {       
        if(planner == null)
        {
            //nothing to update from, so do nothing
        }
        else
        {
            List<String> topicData = new ArrayList<String>();
            for(TopicInterface t : planner.getTopics())
            {
                topicData.add(t.getSubject() + " (" + t.getDuration() + ")" );                
            }
            topicList.setListData(topicData.toArray(new String[1]));
            
            // Create the list of study blocks that goes into the gui box
            SimpleDateFormat df = new SimpleDateFormat("dd/MM  HH:mm") ;
            List<String> eventData = new ArrayList<String>();
            
            for(StudyBlockInterface s : planner.getStudyPlan())
            {
            	eventData.add( df.format(s.getStartTime().getTime()) + ") " + s.getTopic() + "    " + s.getDuration() + " min.") ;
            }
            studyPlan.setListData(eventData.toArray(new String[1]));
            
            // Create the list of events that is shown in the gui (only events of types EXAM and ESSAY)
            List<String> events = new ArrayList<String>() ;
            for (CalendarEventInterface c : planner.getCalendarEvents()) {
            	if (c.isValidTopicTarget()) {
            		events.add( c.getName() + "    (" + df.format(c.getStartTime().getTime()) + "  " + c.getDuration() +"min)" ) ;
            	}
            }
            eventList.setListData(events.toArray(new String[1]));
        }
        
    }
    
    
    
    public static void main(String[] args)
    {
        StudyPlanner planner = new StudyPlanner(); 
        planner.addTopic("Java file handling", 480);
        planner.addTopic("European agricultural policy 1950-1974", 720);
        
        StudyPlannerGUI gui = new StudyPlannerGUI(planner);
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.setSize(1200,700);
        
        planner.setGUI(gui);
                
        gui.setVisible(true);
        gui.updateDisplay();
    }

}
