package japrc2013;

public class StudyPlannerException extends RuntimeException
{
    public StudyPlannerException(String message)
    {
        super(message);
    }
}
