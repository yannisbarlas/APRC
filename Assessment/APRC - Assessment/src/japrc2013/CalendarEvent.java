// Examination Number : Y1448283

package japrc2013;

import japrc2013.StudyPlannerInterface.CalendarEventType;

import java.util.Calendar;

public class CalendarEvent implements CalendarEventInterface {

	private String name ;
	private Calendar startTime ;
	private int duration ;
	private CalendarEventType type ;
	
	public CalendarEvent (String n, Calendar s, int d, CalendarEventType t) {
		name 	  = n ;
		startTime = s ;
		duration  = d ;
		type 	  = t ;
	}
	
	@Override
	public String getName() { return name; }

	@Override
	public Calendar getStartTime() { return startTime; }

	@Override
	public int getDuration() { return duration; }

	@Override
	public boolean isValidTopicTarget() {
		// If event type is either EXAM or ESSAY, then it is a valid topic target
		if (type.equals(CalendarEventType.EXAM) || type.equals(CalendarEventType.ESSAY)) {
			return true ;
		}
		return false;
	}

}
