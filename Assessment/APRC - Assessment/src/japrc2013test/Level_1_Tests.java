// Examination Number : Y1448283

package japrc2013test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import japrc2013.StudyBlockInterface;
import japrc2013.StudyPlanner;
import japrc2013.StudyPlannerInterface;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class Level_1_Tests {
	
	private StudyPlannerInterface planner ;
	private Calendar c ;
	
	@Before
	public void setUp() throws Exception
    {
		planner = new StudyPlanner() ;
    }
	
	// Check that generateStudy(Calendar startStudy) works correctly
	@Test
	public void testStartTime() {
		// Create calendar and give it a specific start time
		c = Calendar.getInstance() ;
		c.set(Calendar.HOUR_OF_DAY, 9);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		// Add two topics
		planner.addTopic("name", 300);
		planner.addTopic("name2", 160) ;
		
		// Set durations
		planner.setBlockSize(60);
		planner.setBreakLength(10);
		
		// Generate plan
		planner.generateStudyPlan(c);
		
		// Check that the plan begins at 9.00 
		SimpleDateFormat df = new SimpleDateFormat("HH:mm") ;
		String beginTime = df.format(planner.getStudyPlan().get(0).getStartTime().getTime()) ;
		assertEquals(beginTime, "09:00") ;
		
		// Check the first break begins 60 minutes later, at 10.00 
		String secondTime = df.format(planner.getStudyPlan().get(1).getStartTime().getTime()) ;
		assertEquals(secondTime, "10:00") ;
		
		// Check that the second study block begins 10 minutes after the break started, at 10.10
		String thirdTime = df.format(planner.getStudyPlan().get(2).getStartTime().getTime()) ;
		assertEquals(thirdTime, "10:10") ;
		
		// Check that the first topic is the topic in both of the last studyblocks
		String end1 = planner.getStudyPlan().get(12).getTopic() ;
		String end2 = planner.getStudyPlan().get(14).getTopic() ;
		assertEquals(end1,end2);
	}
	
	// Tests that the last entry of a topic (if duration % blockSize != 0) is shorter than the standard block duration
	@Test
	public void testShorterLastEntry() {
		// Create calendar and give it a specific start time
		c = Calendar.getInstance() ;
		c.set(Calendar.HOUR_OF_DAY, 9);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		// Set durations
		planner.setBlockSize(60);
		planner.setBreakLength(0);
		
		// Store some values that are not divisible by 60
		int[] values = {652,841,753,954,877,112,322,115,564,457,84,97,431,945} ;
		
		int d = 0 ;
		
		for (int v : values) {
			
			planner.getTopics().clear();
			
			// Make two topics
			planner.addTopic("name1", 720);
			planner.addTopic("name2", v);
			
			planner.generateStudyPlan();
			
			// Get the second topic's last entry's duration
			for (StudyBlockInterface s : planner.getStudyPlan()) {
				if (s.getTopic() == "name2") { d = s.getDuration() ; }
			}

			// Check that the second topic has a last entry of less than 60 minutes
			assertTrue(d < 60);
		}
	}
	
	// Repeatedly gives random values to block size, break length and topic durations and checks that breaks appear where expected
	@Test
	public void testBreaksWithRandomValues() {
		
		// Create calendar and give it a specific start time
		c = Calendar.getInstance() ;
		c.set(Calendar.HOUR_OF_DAY, 9);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		
		int i = 0 ;
		
		// While loop also ensures that generate study plan method can be called repeatedly
		while (i<100) {
			
			// Get random topic durations
			int first  = 1 + (int)(Math.random()*(1111-1)) ;
			int second = 1 + (int)(Math.random()*(1111-1)) ;
			
			// Add two topics
			planner.addTopic("name" , first)	;
			planner.addTopic("name2", second) 	;
			
			// Get and set random block and break sizes
			int block  = 10 + (int)(Math.random()*(500-10)) ;
			int breaks = (int)(Math.random()* 60 ) 			;
			
			planner.setBlockSize(block)		;
			planner.setBreakLength(breaks)	;
			
			// Generate plan
			planner.generateStudyPlan(c);
			
			// Check that the last entry in the planner is not a break
			assertTrue(! planner.getStudyPlan().get(planner.getStudyPlan().size()-1).getTopic().equals("break")) ;

			for (int j=0 ; j<planner.getStudyPlan().size() ; j++) {
				if (breaks!=0 && block<480) {
					// Check that the first entry is the first topic
					assertEquals(planner.getStudyPlan().get(0).getTopic(), planner.getTopics().get(0).getSubject()) ;
					
					// Check that the second entry is a break
					assertEquals(planner.getStudyPlan().get(1).getTopic(), "break") ;
					
					// Check that within a day, every other entry is a break
					// If it's not the first or the last entry of the planner
					if (j>0 && j<planner.getStudyPlan().size()-1) {
						// If we are in the same day
						if (planner.getStudyPlan().get(j).getStartTime().get(Calendar.DAY_OF_MONTH) == planner.getStudyPlan().get(j+1).getStartTime().get(Calendar.DAY_OF_MONTH)) {
							// If we have a topic and not a break
							if (planner.getStudyPlan().get(j).getTopic() != "break") {
								// If it's not the first entry in the day
								if (planner.getStudyPlan().get(j).getStartTime().get(Calendar.DAY_OF_MONTH) == planner.getStudyPlan().get(j-1).getStartTime().get(Calendar.DAY_OF_MONTH)) {
									// Check that the entry right before and right after is a break
									assertTrue(planner.getStudyPlan().get(j-1).getTopic().equals("break"));
									assertTrue(planner.getStudyPlan().get(j+1).getTopic().equals("break"));
								}
								// If it is the first entry in the day
								else {
									// Check that the entry right after is a break
									assertTrue(planner.getStudyPlan().get(j+1).getTopic().equals("break")) ;
					}}}}
			}} // end for loop
			
			i++ ;
			planner.getTopics().clear() ;
		} // end while loop
	}
}


