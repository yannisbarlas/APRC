// Examination Number : Y1448283

package japrc2013test;

import static org.junit.Assert.*;
import japrc2013.StudyBlockInterface;
import japrc2013.StudyPlanner;
import japrc2013.StudyPlannerInterface;
import japrc2013.StudyPlannerInterface.CalendarEventType;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

public class Level_2_Tests {

	private StudyPlannerInterface planner ;
	private Calendar c ;
	
	@Before
	public void setUp() throws Exception
    {
		planner = new StudyPlanner() ;
    }
	
	
	// Gets random values for daily start and end study time, and test their behaviour
	@Test
	public void testStartAndEndTimeOfDay() {
		int startHour, startMinute, endHour, endMinute ;
		int day = 0 ;
		int d = 0 	;
		int i = 0 	;
		
		while (i<100) {
			planner.setBlockSize(60);
			planner.setBreakLength(0);
			
			// Set calendar start time and end time to 0.00 and 23.59, so that there are no conflicts when setting them
			Calendar st = Calendar.getInstance() ;
			setTime(st,0,0) ;
			Calendar en = Calendar.getInstance() ;
			setTime(en,23,59) ;
			planner.setDailyStartStudyTime(st);
			planner.setDailyEndStudyTime(en);
			
			// Get random values for daily start time and end time
			startHour    = (int)(Math.random()* 23) ;
			startMinute  = (int)(Math.random()* 60) ;
			endHour = (startHour+1) + (int)(Math.random()*(23-(startHour+1))) ;
			if (startHour == endHour-1) { endMinute = startMinute + (int)(Math.random()*(60-startMinute)) ; }
			else { endMinute = (int)(Math.random()* 60) ; }
			
			// Make calendars with the random values and update start and end times
			Calendar a = Calendar.getInstance() ;
			setTime(a, startHour, startMinute) ;
			
			Calendar b = Calendar.getInstance() ;
			setTime(b, endHour, endMinute) ;
			
			planner.setDailyStartStudyTime(a);
			planner.setDailyEndStudyTime(b);
			
			// Add topics
			planner.addTopic("Java file handling", 480);
	        planner.addTopic("European agricultural policy 1950-1974", 720);   
			
	        // Generate plan
	        c = Calendar.getInstance() ;
	        setTime(c, 0, 0);
	        
			planner.generateStudyPlan(c);
			
			// Get value of first day
			day = planner.getStudyPlan().get(0).getStartTime().get(Calendar.DAY_OF_MONTH) ;
			
			
			for (StudyBlockInterface s : planner.getStudyPlan()) {
				// Only check the first and second day
				// Topics start to run out of study minutes after that, distorting the daily end time
				if (s.getStartTime().get(Calendar.DAY_OF_MONTH) == day || s.getStartTime().get(Calendar.DAY_OF_MONTH) == day + 1) {
					// If s is the first entry in the study plan, check that it is the correct time (ie. daily start study time)
					if (planner.getStudyPlan().indexOf(s) == 0) {
						assertTrue(s.getStartTime().compareTo(a) == 0) ;
					}
					// If s is the first entry for a day in the study plan
					else if (s.getStartTime().get(Calendar.DAY_OF_MONTH) != planner.getStudyPlan().get(planner.getStudyPlan().indexOf(s) -1).getStartTime().get(Calendar.DAY_OF_MONTH)) {
						
						// Check that the day began at daily start study time
						assertTrue(s.getStartTime().get(Calendar.HOUR_OF_DAY) == a.get(Calendar.HOUR_OF_DAY)) ;
						assertTrue(s.getStartTime().get(Calendar.MINUTE) == a.get(Calendar.MINUTE)) ;
						
						// Make a calendar with the previous day's end time
						Calendar cal = planner.getStudyPlan().get(planner.getStudyPlan().indexOf(s) -1).getStartTime() ;
						cal.add(Calendar.MINUTE, planner.getStudyPlan().get(planner.getStudyPlan().indexOf(s) -1).getDuration()) ;
						
						// Check that the previous day ended at daily end study time
						if (endMinute < startMinute) { d = 60 + endMinute - startMinute ; }
						else { d = endMinute - startMinute ; }
						
						// For example, if start time is 10:59 and end time is 17:01, day will end at 16.59
						if (d<10 && startMinute>10 && endMinute<10) {
							assertTrue(cal.get(Calendar.MINUTE) == startMinute) ;
							assertTrue(cal.get(Calendar.HOUR_OF_DAY) == endHour-1) ;
						}
						// For example, if start time is 12.07 and end time is 15.12, day will end at 15.07
						else if (d<10) { 
							assertTrue(cal.get(Calendar.MINUTE) == startMinute) ;
							assertTrue(cal.get(Calendar.HOUR_OF_DAY) == endHour) ;
						}
						// Case where day will end at exactly the end time
						else {
							assertTrue(cal.get(Calendar.MINUTE) == endMinute) ;
							assertTrue(cal.get(Calendar.HOUR_OF_DAY) == endHour) ;
						}
					}
				}
			} // end for loop
			
			i++ ;
			planner.getTopics().clear();
			
		} // end while loop
	}
	
	
	@Test
	// Checks that if there is not enough time left in a day for a full study block, a shorter one will be scheduled
	public void testShorterBlock() {
		// Set start calendar
		c = Calendar.getInstance() ;
		setTime(c, 8, 0);
		
		// Add topics
		planner.addTopic("Java file handling", 480);
        planner.addTopic("European agricultural policy 1950-1974", 720);  
        
        // Set block size and break length
        planner.setBlockSize(120);
        planner.setBreakLength(15);
        
        planner.generateStudyPlan(c);
        
        // In the planner
        for (int i=1 ; i<planner.getStudyPlan().size() ; i++) {
        	// If the day changes
        	if (planner.getStudyPlan().get(i-1).getStartTime().get(Calendar.DAY_OF_MONTH) != planner.getStudyPlan().get(i).getStartTime().get(Calendar.DAY_OF_MONTH)) {
        		// Get the duration of the last topic from the previous day and check its value
        		int dur = planner.getStudyPlan().get(i-1).getDuration() ;
        		assertTrue(dur < 120) ;
        	}
        }
	}
	
	
	@Test
	// Make a lot of events of different durations and various types, preferably at times that could cause a problem
	// Test that they work as expected
	public void testEvents() {
		int pos = 0, dur=0 ;
		SimpleDateFormat df = new SimpleDateFormat("HH:mm") ;
		
		c = Calendar.getInstance() 				;
		setTime(c,8,0) 							;
		int day = c.get(Calendar.DAY_OF_MONTH) 	;
		
		// Add topics
		planner.addTopic("Java file handling", 480)						;
        planner.addTopic("European agricultural policy 1950-1974", 720)	;   
        
        // Add an event before the start of the first day
        Calendar d = Calendar.getInstance() ;
        setTime(d,6,0) ;
        planner.addCalendarEvent("one", d, 60);
        
        // Add two events in a row in the middle of the first day
        Calendar e = Calendar.getInstance() ;
        setTime(e,13,0) ;
        planner.addCalendarEvent("two", e, 50);
        
        Calendar f = Calendar.getInstance() ;
        setTime(f,13,50);
        planner.addCalendarEvent("three", f, 10);
        
        // Add an event after end time of the first day
        Calendar g = Calendar.getInstance() ;
        setTime(g,18,30) ;
        planner.addCalendarEvent("four", g, 60, CalendarEventType.EXAM);
        
        // Add an event overlapping with the start of the second day
        Calendar h = Calendar.getInstance() ;
        setTime(h,8,30);
        h.add(Calendar.HOUR_OF_DAY, 24);
        planner.addCalendarEvent("five", h, 92, CalendarEventType.ESSAY);
        
        // Add an event in the middle of the second day (also check that events of duration smaller than the minimum block size can be created)
        Calendar i = Calendar.getInstance() ;
        setTime(i,14,52);
        i.add(Calendar.HOUR_OF_DAY, 24);
        planner.addCalendarEvent("six", i, 7);
        
        // Add an event overlapping with the end of the second day
        Calendar j = Calendar.getInstance() ;
        setTime(j,16,38);
        j.add(Calendar.HOUR_OF_DAY, 24);
        planner.addCalendarEvent("seven", j, 44, CalendarEventType.EXAM);
        
        // Add an event in the third day that starts before the beginning of study time and ends after the end of study time
        Calendar k = Calendar.getInstance() ;
        setTime(k,8,30);
        k.add(Calendar.HOUR_OF_DAY, 48);
        planner.addCalendarEvent("eight", k, 560);
        
        
        planner.generateStudyPlan(c);
        
        for (StudyBlockInterface s : planner.getStudyPlan()) {
        	switch (s.getTopic()) {
	        	case "one" :
	        		// Check that the event starts at 6am
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("06:00")) ;
	        		// Check that it is not followed by a break (as it ends before saily study time begins)
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(! planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		// Check that we are in the first day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day) ;
	        		break ;
	        		
	        	case "two" :
	        		// Check that the event starts at 1pm
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("13:00")) ;
	        		// Check that it is not followed by a break (as it is followed directly by another event)
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(! planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		// Check that we are in the first day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day) ;
	        		break ;
	        		
	        	case "three" :
	        		// Check that the event starts at 1pm
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("13:50")) ;
	        		// Check that it is followed by a break
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		// Check that we are in the first day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day) ;
	        		break ;
	        		
	        	case "four" :
	        		// Check that the event starts at 1pm
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("18:30")) ;
	        		// Check that it is not followed by a break (it is after the end of daily study time)
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(! planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		// Check that the previous entry ended at 5pm
	        		pos = planner.getStudyPlan().indexOf(s) - 1 ;
	        		Calendar v = planner.getStudyPlan().get(pos).getStartTime() ;
	        		dur = planner.getStudyPlan().get(pos).getDuration() ;
	        		v.add(Calendar.MINUTE, dur);
	        		assertTrue(df.format(v.getTime()).equals("17:00")) ;
	        		// Check that we are in the first day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day) ;
	        		break ;
	        		
	        	case "five" :
	        		// Check that the event starts at 8.30am
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("08:30")) ;
	        		// Check that it is followed by a break that starts at 10.02 am
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		assertTrue(df.format(planner.getStudyPlan().get(pos).getStartTime().getTime()).equals("10:02"));
	        		// Check that we are in the second day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day + 1) ;
	        		break;
	        		
	        	case "six" :
	        		// Check that the event starts at 2.52pm
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("14:52")) ;
	        		// Check that it is followed by a break that starts at 2.59pm
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		assertTrue(df.format(planner.getStudyPlan().get(pos).getStartTime().getTime()).equals("14:59"));
	        		// Check that we are in the second day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day + 1) ;
	        		break;
	        		
	        	case "seven" :
	        		// Check that the event starts at 4.52pm
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("16:38")) ;
	        		// Check that it is not followed by a break
	        		pos = planner.getStudyPlan().indexOf(s) + 1 ;
	        		assertTrue(! planner.getStudyPlan().get(pos).getTopic().equals("break")) ;
	        		// Check that we are in the second day
	        		assertTrue(s.getStartTime().get(Calendar.DAY_OF_MONTH) == day + 1) ;
	        		break;
	        		
	        	case "eight" :
	        		// Check that the event starts at 8.30pm
	        		assertTrue(df.format(s.getStartTime().getTime()).equals("08:30")) ;
	        		// Check that it is the only thing planned for the day
	        		int z = 0 ;
	        		for (StudyBlockInterface sb : planner.getStudyPlan()) {
	        			if (sb.getStartTime().get(Calendar.DAY_OF_MONTH) == day + 2) { 
	        				z++ ;
	        				if (! sb.getTopic().equals("eight")) { fail() ; }
	        			}
	        		}
	        		assertTrue(z==1) ;
	        		break;
        	}
        }
	}
	
	
	// Helper function to reduce code repetition
	private void setTime(Calendar calendar, int hour, int minute) {
		calendar.set(Calendar.HOUR_OF_DAY, hour) ;
		calendar.set(Calendar.MINUTE, minute) ;
		calendar.set(Calendar.SECOND, 0) ;
		calendar.set(Calendar.MILLISECOND, 0) ;
	}
	
}
