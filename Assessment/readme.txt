All durations are in minutes.

The "generate study plan" button creates a study plan from the current time.

Clicking on all text fields should delete the text given. That text is only there as an indication of 
what the user should enter in each field.

After giving a valid name and duration, if the user clicks the "add" button, the new topic and its duration 
should appear on the list at the top of the window.

If a topic from that list has been selected and the "delete" button is clicked, that topic is deleted from the list.
This only works for one topic at a time.

Given valid input, if the "update durations" button is clicked, the values for block size and break length will change.

After adding or deleting a topic, or updating durations, the plan is not automatically regenerated.

The study plan displays each block with its date first, then its time, its topic and duration.

When creating a new calendar event, the user needs to pay attention in order to give the event date and time
in the correct format. The user is also provided with a list of three types to choose from for the event.

The list of events is only there to let the user set a target event for a topic. For this reason, only valid topic targets
are shown (eg. an event of type OTHER is not listed).

"Save plan" and "load plan" buttons take the user to the project's source folder.
--------------------------------------------------------------------------------------------------------------------------------------------------